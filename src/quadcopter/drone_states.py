#!/usr/bin/env python


# Fake enum of states the drone reports
class DroneStates(object):
    Emergency = 0
    Inited = 1
    Landed = 2
    Flying = 3
    Hovering = 4
    Test = 5
    TakingOff = 6
    GotoHover = 7
    Landing = 8
    Looping = 9

    StatusMessages = {
        Emergency: 'Emergency',
        Inited: 'Initialized',
        Landed: 'Landed',
        Flying: 'Flying',
        Hovering: 'Hovering',
        Test: 'Test (?)',
        TakingOff: 'Taking Off',
        GotoHover: 'Going to Hover Mode',
        Landing: 'Landing',
        Looping: 'Looping (?)'
    }
