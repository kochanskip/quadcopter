#!/usr/bin/env python
import unittest
import time

from ardrone_autonomy.msg import Navdata
from geometry_msgs.msg import Twist
from std_srvs.srv import Empty as srv_Empty
import rospy
import rostest


class TestNavDataSplitterToMultiplexer(unittest.TestCase):
    def setUp(self):
        rospy.init_node('TestNavDataSplitterToMultiplexer')
        self.ns = rospy.get_namespace()
        self.pubNavData = rospy.Publisher(self.ns + 'ardrone/navdata', Navdata, queue_size=1)
        self.subMoveCom = rospy.Subscriber(self.ns + "quadcopter/commands/move", Twist, self.receive_data)

        rospy.wait_for_service(self.ns + 'quadcopter/pid/reset_roll')
        self.resetRoll = rospy.ServiceProxy(self.ns + 'quadcopter/pid/reset_roll', srv_Empty)

        rospy.wait_for_service(self.ns + 'quadcopter/pid/reset_pitch')
        self.resetPitch = rospy.ServiceProxy(self.ns + 'quadcopter/pid/reset_pitch', srv_Empty)

        rospy.wait_for_service(self.ns + 'quadcopter/pid/reset_height')
        self.resetHeight = rospy.ServiceProxy(self.ns + 'quadcopter/pid/reset_height', srv_Empty)

        rospy.wait_for_service(self.ns + 'quadcopter/pid/reset_yaw')
        self.resetYaw = rospy.ServiceProxy(self.ns + 'quadcopter/pid/reset_yaw', srv_Empty)

        self.navdata = Navdata()
        self.receivedCommand = Twist()
        self.expectedCommand = Twist()
        self.received = False

        # Allowing set up time for the multiplexer
        rospy.sleep(0.5)

    def receive_data(self, sub_command):
        self.receivedCommand = sub_command
        self.received = True

    def test_1_when_input_data_is_equal_to_set_points_then_receive_neutral_command_directions(self):
        self.navdata.tags_count = 1
        self.navdata.tags_xc = [500]  # roll
        self.navdata.tags_yc = [500]  # height
        self.navdata.tags_distance = [100.0]  # pitch
        self.navdata.rotZ = 0.0  # yaw

        rospy.loginfo(self.navdata)
        self.pubNavData.publish(self.navdata)

        # roll goes to linear.y
        # height goes to linear.z
        # pitch goes to linear.x
        # yaw goes to angular.z
        self.expectedCommand.linear.x = 0.0
        self.expectedCommand.linear.y = 0.0
        self.expectedCommand.linear.z = 0.0
        self.expectedCommand.angular.z = 0.0

        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 2)
            self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 2)
            self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 2)
            self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 2)
            self.assertEqual(self.receivedCommand, self.expectedCommand)
        else:
            self.assertTrue(False, msg="Never received command from the multiplexer, timed out.")

    def test_2_when_input_data_is_less_than_set_points_then_positive_command_directions_given(self):
        self.navdata.tags_count = 1
        self.navdata.tags_xc = [300]  # roll
        self.navdata.tags_yc = [300]  # height
        self.navdata.tags_distance = [50.0]  # pitch
        self.navdata.rotZ = 0.0  # yaw

        rospy.loginfo(self.navdata)
        self.pubNavData.publish(self.navdata)

        # roll goes to linear.y
        # height goes to linear.z
        # pitch goes to linear.x
        # yaw goes to angular.z
        self.expectedCommand.linear.x = -0.2
        self.expectedCommand.linear.y = 0.16
        self.expectedCommand.linear.z = 0.16
        self.expectedCommand.angular.z = 0.0

        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 2)
            self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 2)
            self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 2)
            self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 2)
            self.assertEqual(self.receivedCommand, self.expectedCommand)
        else:
            self.assertTrue(False, msg="Never received command from the multiplexer, timed out.")

    def test_3_when_input_data_is_greater_than_set_points_then_negative_command_directions_given(self):
        self.navdata.tags_count = 1
        self.navdata.tags_xc = [700]  # roll
        self.navdata.tags_yc = [700]  # height
        self.navdata.tags_distance = [150.0]  # pitch
        self.navdata.rotZ = 0.0  # yaw

        rospy.loginfo(self.navdata)
        self.pubNavData.publish(self.navdata)

        # roll goes to linear.y
        # height goes to linear.z
        # pitch goes to linear.x
        # yaw goes to angular.z
        self.expectedCommand.linear.x = 0.22
        self.expectedCommand.linear.y = -0.18
        self.expectedCommand.linear.z = -0.18
        self.expectedCommand.angular.z = 0.0

        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 2)
            self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 2)
            self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 2)
            self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 2)
            self.assertEqual(self.receivedCommand, self.expectedCommand)
        else:
            self.assertTrue(False, msg="Never received command from the multiplexer, timed out.")

    def test_4_when_input_data_is_back_equal_to_set_points_then_negative_command_directions_given(self):
        self.navdata.tags_count = 1
        self.navdata.tags_xc = [500]  # roll
        self.navdata.tags_yc = [500]  # height
        self.navdata.tags_distance = [100.0]  # pitch
        self.navdata.rotZ = 0.0  # yaw

        rospy.loginfo(self.navdata)
        self.pubNavData.publish(self.navdata)

        # roll goes to linear.y
        # height goes to linear.z
        # pitch goes to linear.x
        # yaw goes to angular.z
        self.expectedCommand.linear.x = -0.05
        self.expectedCommand.linear.y = 0.04
        self.expectedCommand.linear.z = 0.04
        self.expectedCommand.angular.z = 0.0

        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 2)
            self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 2)
            self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 2)
            self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 2)
            self.assertEqual(self.receivedCommand, self.expectedCommand)
        else:
            self.assertTrue(False, msg="Never received command from the multiplexer, timed out.")

    def test_5_when_input_data_is_set_to_max_then_negative_command_directions_given(self):
        self.navdata.tags_count = 1
        self.navdata.tags_xc = [1000]  # roll
        self.navdata.tags_yc = [1000]  # height
        self.navdata.tags_distance = [200.0]  # pitch
        self.navdata.rotZ = 0.0  # yaw

        rospy.loginfo(self.navdata)
        self.pubNavData.publish(self.navdata)

        # roll goes to linear.y
        # height goes to linear.z
        # pitch goes to linear.x
        # yaw goes to angular.z
        self.expectedCommand.linear.x = 0.4
        self.expectedCommand.linear.y = -0.4
        self.expectedCommand.linear.z = -0.4
        self.expectedCommand.angular.z = 0.0

        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 2)
            self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 2)
            self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 2)
            self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 2)
            self.assertEqual(self.receivedCommand, self.expectedCommand)
        else:
            self.assertTrue(False, msg="Never received command from the multiplexer, timed out.")

    def test_6_when_input_data_is_set_to_min_then_positive_command_directions_given(self):
        self.navdata.tags_count = 1
        self.navdata.tags_xc = [0]  # roll
        self.navdata.tags_yc = [0]  # height
        self.navdata.tags_distance = [0.0]  # pitch
        self.navdata.rotZ = 0.0  # yaw

        rospy.loginfo(self.navdata)
        self.pubNavData.publish(self.navdata)

        # roll goes to linear.y
        # height goes to linear.z
        # pitch goes to linear.x
        # yaw goes to angular.z
        self.expectedCommand.linear.x = -0.45
        self.expectedCommand.linear.y = 0.45
        self.expectedCommand.linear.z = 0.45
        self.expectedCommand.angular.z = 0.0

        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 2)
            self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 2)
            self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 2)
            self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 2)
            self.assertEqual(self.receivedCommand, self.expectedCommand)
        else:
            self.assertTrue(False, msg="Never received command from the multiplexer, timed out.")

    def test_7_when_no_tags_are_visible_then_get_directions_based_on_previous_test(self):
        self.navdata.tags_count = 0
        self.navdata.tags_xc = []  # roll
        self.navdata.tags_yc = []  # height
        self.navdata.tags_distance = []  # pitch
        self.navdata.rotZ = 0.0  # yaw

        rospy.loginfo(self.navdata)
        self.pubNavData.publish(self.navdata)

        # roll goes to linear.y
        # height goes to linear.z
        # pitch goes to linear.x
        self.expectedCommand.linear.x = -0.3
        self.expectedCommand.linear.y = 0.3
        self.expectedCommand.linear.z = 0.3
        self.expectedCommand.angular.z = 0.0

        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 2)
            self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 2)
            self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 2)
            self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 2)
            self.assertEqual(self.receivedCommand, self.expectedCommand)
        else:
            self.assertTrue(False, msg="Never received command from the multiplexer, timed out.")

    def test_8_when_using_publishing_ten_nav_data_in_a_loop(self):
        self.resetRoll()
        self.resetPitch()
        self.resetHeight()
        self.resetYaw()

        counter = 0
        roll = 500
        height = 500
        pitch = 100.0
        yaw = 0
        # not sure if yaw is right...

        # roll goes to linear.y
        # height goes to linear.z
        # pitch goes to linear.x
        # yaw goes to angular.z
        expected_results = [(0.0, 0.0, 0.0, 0.0),
                            # (0.02, -0.008, -0.008),
                            # (0.038, -0.015, -0.015),
                            (0.058, -0.023, -0.023, 0.0),
                            (0.08, -0.032, -0.032, 0.0),
                            (0.105, -0.042, -0.042, 0.0),
                            (0.132, -0.053, -0.053, 0.0),
                            (0.162, -0.065, -0.065, 0.0),
                            (0.195, -0.078, -0.078, 0.0),
                            (0.23, -0.092, -0.092, 0.0)]

        results = []

        while counter < 10:
            self.navdata.tags_count = 1
            self.navdata.tags_xc = [roll]  # roll
            self.navdata.tags_yc = [height]  # height
            self.navdata.tags_distance = [pitch]  # pitch
            self.navdata.rotZ = 0.0  # yaw

            rospy.loginfo(self.navdata)
            self.pubNavData.publish(self.navdata)

            roll += 10
            height += 10
            pitch += 5
            yaw += 0
            # not sure

            start_time = time.time()
            while not self.received and not time.time() > start_time + 2:
                rospy.sleep(0.5)
            if self.received:
                self.received = False
                self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 3)
                self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 3)
                self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 3)
                self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 2)
                results.append((float(self.receivedCommand.linear.x), float(self.receivedCommand.linear.y),
                                float(self.receivedCommand.linear.z), float(self.receivedCommand.angular.z)))
            else:
                if self.receivedCommand.linear.x == 0.0 and self.receivedCommand.linear.y == 0.0 \
                        and self.receivedCommand.linear.z == 0.0 and self.receivedCommand.angular.z == 0.0:
                    self.assertTrue(True, msg="Didn't receive command, assuming ignored by deadband")
                else:
                    self.assertTrue(False, msg="Never received command from the multiplexer, timed out.")

            rospy.sleep(0.05)
            counter += 1

        self.assertEqual(results, expected_results)

    def test_9_when_using_publishing_empty_nav_data_in_a_loop(self):
        self.resetRoll()
        self.resetPitch()
        self.resetHeight()
        self.resetYaw()

        counter = 0

        # roll goes to linear.y
        # height goes to linear.z
        # pitch goes to linear.x
        # yaw goes to angular.z
        expected_results = [(0.0, 0.0, 0.0, 0.0)]

        results = []

        while counter < 5:
            self.navdata.tags_count = 0
            self.navdata.tags_xc = []  # roll
            self.navdata.tags_yc = []  # height
            self.navdata.tags_distance = []  # pitch
            self.navdata.rotZ = 0.0  # yaw

            rospy.loginfo(self.navdata)
            self.pubNavData.publish(self.navdata)

            start_time = time.time()
            while not self.received and not time.time() > start_time + 2:
                rospy.sleep(0.5)
            if self.received:
                self.received = False
                self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 3)
                self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 3)
                self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 3)
                self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 3)
                results.append((float(self.receivedCommand.linear.x), float(self.receivedCommand.linear.y),
                                float(self.receivedCommand.linear.z), float(self.receivedCommand.angular.z)))
            else:
                if self.receivedCommand.linear.x == 0.0 and self.receivedCommand.linear.y == 0.0 \
                        and self.receivedCommand.linear.z == 0.0 and self.receivedCommand.angular.z == 0.0:
                    self.assertTrue(True, msg="Didn't receive command, assuming ignored by deadband")
                else:
                    self.assertTrue(False, msg="Never received command from the multiplexer, timed out.")

            rospy.sleep(0.05)
            counter += 1

        self.assertEqual(results, expected_results)


if __name__ == '__main__':
    rostest.rosrun("src", "test_navdata_splitter_to_multiplexer", TestNavDataSplitterToMultiplexer)