#!/usr/bin/env python
import unittest
import time
import os

import rospy
import rostest
from cv_bridge import CvBridge
from sensor_msgs.msg import Image
from std_msgs.msg import Float32

import cv2


class TestOpenCV(unittest.TestCase):
    def setUp(self):
        rospy.init_node('TestOpenCV')
        self.ns = rospy.get_namespace()
        self.pubNavData = rospy.Publisher(self.ns + "quadcopter/image/image_rect_color", Image, queue_size=1)
        self.subPIDDataRoll = rospy.Subscriber(self.ns + "quadcopter/tag_data/roll", Float32, self.receive_data_roll)
        self.subPIDDataHeight = rospy.Subscriber(self.ns + "quadcopter/tag_data/height", Float32,
                                                 self.receive_data_height)
        self.subPIDDataPitch = rospy.Subscriber(self.ns + "quadcopter/tag_data/pitch", Float32, self.receive_data_pitch)

        self.bridge = CvBridge()
        self.receivedCommandRoll = 0.0
        self.receivedCommandHeight = 0.0
        self.receivedCommandPitch = 0.0
        self.receivedRoll = False
        self.receivedHeight = False
        self.receivedPitch = False

        # Allowing set up time for the multiplexer
        rospy.sleep(0.5)

    def receive_data_roll(self, data):
        self.receivedCommandRoll = data.data
        self.receivedRoll = True

    def receive_data_height(self, data):
        self.receivedCommandHeight = data.data
        self.receivedHeight = True

    def receive_data_pitch(self, data):
        self.receivedCommandPitch = data.data
        self.receivedPitch = True

    def create_ros_image(self, image):
        rospy.loginfo(str(os.getcwd()))
        print str(os.getcwd())
        cv2_image = cv2.imread("../catkin_ws/src/quadcopter/src/quadcopter/opencv/test/test_images/" + image,
                               cv2.IMREAD_UNCHANGED)
        return self.bridge.cv2_to_imgmsg(cv2_image, "bgr8")

    def in_range(self, minimum, maximum, value):
        return minimum <= value <= maximum

    def compare_data(self, command_roll, command_height, command_pitch, expected_true=True):
        start_time = time.time()
        while (not (self.receivedRoll and self.receivedHeight and self.receivedPitch)) \
                and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.receivedRoll and self.receivedHeight and self.receivedPitch:
            self.receivedRoll = False
            self.receivedHeight = False
            self.receivedPitch = False
            rospy.loginfo("roll: " + str(self.receivedCommandRoll))
            rospy.loginfo("height: " + str(self.receivedCommandHeight))
            rospy.loginfo("pitch: " + str(self.receivedCommandPitch))
            if expected_true:
                self.assertTrue(self.in_range(command_roll[0], command_roll[1], self.receivedCommandRoll) and
                                self.in_range(command_height[0], command_height[1], self.receivedCommandHeight) and
                                self.in_range(command_pitch[0], command_pitch[1], self.receivedCommandPitch),
                                msg="One or more values not in range")
            else:
                self.assertFalse(self.in_range(command_roll[0], command_roll[1], self.receivedCommandRoll) and
                                 self.in_range(command_height[0], command_height[1], self.receivedCommandHeight) and
                                 self.in_range(command_pitch[0], command_pitch[1], self.receivedCommandPitch),
                                 msg="One or more values not in range")
        else:
            self.assertTrue(False, msg="Never received all topic data from opencv, timed out.")

    #
    def account_for_focal_change(self, value):
        return value * 0.75

    def test_opencv_when_tag_is_not_present_then_receive_minus_one_on_topics(self):
        # Can't convert jpg image to a ROS image, so converting it to an OpenCV format and then to a ROS image.
        ros_image = self.create_ros_image("no_tag.jpg")
        self.pubNavData.publish(ros_image)
        rospy.loginfo("test_opencv_when_tag_is_not_present_then_receive_minus_one_on_topics")
        self.compare_data((-1.0, -1.0), (-1.0, -1.0), (-1.0, -1.0))

    def test_opencv_when_tag_is_central_50cm(self):
        # Can't convert jpg image to a ROS image, so converting it to an OpenCV format and then to a ROS image.
        ros_image = self.create_ros_image("tag_central_50.jpg")
        self.pubNavData.publish(ros_image)
        rospy.loginfo("test_opencv_when_tag_is_central_50cm")
        self.compare_data((280.0, 379.0), (183.0, 218.0), (self.account_for_focal_change(48.0),
                                                           self.account_for_focal_change(52.0)))

    def test_opencv_when_tag_is_central_100cm(self):
        # Can't convert jpg image to a ROS image, so converting it to an OpenCV format and then to a ROS image.
        ros_image = self.create_ros_image("tag_central_100.jpg")
        self.pubNavData.publish(ros_image)
        rospy.loginfo("test_opencv_when_tag_is_central_100cm")
        self.compare_data((352.0, 400.0), (179.0, 200.0), (self.account_for_focal_change(95.0),
                                                           self.account_for_focal_change(105.0)))

    def test_opencv_when_tag_is_central_150cm(self):
        # Can't convert jpg image to a ROS image, so converting it to an OpenCV format and then to a ROS image.
        ros_image = self.create_ros_image("tag_central_150.jpg")
        self.pubNavData.publish(ros_image)
        rospy.loginfo("test_opencv_when_tag_is_central_150cm")
        self.compare_data((314.0, 347.0), (182.0, 193.0), (self.account_for_focal_change(145.0),
                                                           self.account_for_focal_change(160.0)))

    def test_opencv_when_tag_is_central_200cm(self):
        # Can't convert jpg image to a ROS image, so converting it to an OpenCV format and then to a ROS image.
        ros_image = self.create_ros_image("tag_central_200.jpg")
        self.pubNavData.publish(ros_image)
        rospy.loginfo("test_opencv_when_tag_is_central_200cm")
        self.compare_data((314.0, 338.0), (189.0, 197.0), (self.account_for_focal_change(190.0),
                                                           self.account_for_focal_change(215.0)))

    def test_opencv_when_tag_is_bottom_10cm(self):
        # Can't convert jpg image to a ROS image, so converting it to an OpenCV format and then to a ROS image.
        ros_image = self.create_ros_image("tag_bottom_10.jpg")
        self.pubNavData.publish(ros_image)
        rospy.loginfo("test_opencv_when_tag_is_bottom_10cm")
        self.compare_data((-1.0, -1.0), (-1.0, -1.0), (-1.0, -1.0))

    def test_opencv_when_tag_is_right_50cm(self):
        # Can't convert jpg image to a ROS image, so converting it to an OpenCV format and then to a ROS image.
        ros_image = self.create_ros_image("tag_right_50.jpg")
        self.pubNavData.publish(ros_image)
        rospy.loginfo("test_opencv_when_tag_is_right_50cm")
        self.compare_data((382.0, 473.0), (183.0, 218.0), (self.account_for_focal_change(45.0),
                                                           self.account_for_focal_change(55.0)))

    def test_opencv_when_tag_is_central_no_yellow_50cm(self):
        # Can't convert jpg image to a ROS image, so converting it to an OpenCV format and then to a ROS image.
        ros_image = self.create_ros_image("tag_central_no_yellow_50.jpg")
        self.pubNavData.publish(ros_image)
        rospy.loginfo("test_opencv_when_tag_is_central_no_yellow_50cm")
        self.compare_data((280.0, 376.0), (183.0, 219.0), (self.account_for_focal_change(45.0),
                                                           self.account_for_focal_change(55.0)))

    def test_opencv_when_tag_is_central_no_purple_100cm(self):
        # Can't convert jpg image to a ROS image, so converting it to an OpenCV format and then to a ROS image.
        # This is a real issue. Out by 30 on roll.
        ros_image = self.create_ros_image("tag_central_no_purple_100.jpg")
        self.pubNavData.publish(ros_image)
        rospy.loginfo("tag_central_no_purple_100")
        self.compare_data((304.0, 353.0), (197.0, 211.0), (self.account_for_focal_change(95.0),
                                                           self.account_for_focal_change(115.0)))

    def test_opencv_when_tag_is_central_no_purple_200cm(self):
        # Can't convert jpg image to a ROS image, so converting it to an OpenCV format and then to a ROS image.
        ros_image = self.create_ros_image("tag_central_no_purple_200.jpg")
        self.pubNavData.publish(ros_image)
        rospy.loginfo("tag_central_no_purple_200")
        self.compare_data((316.0, 342.0), (193.0, 207.0), (self.account_for_focal_change(190.0),
                                                           self.account_for_focal_change(215.0)))

    def test_opencv_when_tag_is_central_no_purple_and_no_yellow_200cm(self):
        # Can't convert jpg image to a ROS image, so converting it to an OpenCV format and then to a ROS image.
        ros_image = self.create_ros_image("tag_central_no_purple_and_no_yellow_200.jpg")
        self.pubNavData.publish(ros_image)
        rospy.loginfo("tag_central_no_purple_and_no_yellow_200")
        self.compare_data((-1.0, -1.0), (-1.0, -1.0), (-1.0, -1.0))

    def test_opencv_when_tag_is_central_no_yellow_and_no_green_adjacency_200cm(self):
        # Can't convert jpg image to a ROS image, so converting it to an OpenCV format and then to a ROS image.
        ros_image = self.create_ros_image("tag_central_no_yellow_and_no_green_adjacency_200.jpg")
        self.pubNavData.publish(ros_image)
        rospy.loginfo("tag_central_no_yellow_and_no_green_adjacency_200")
        self.compare_data((-1.0, -1.0), (-1.0, -1.0), (-1.0, -1.0))

    def test_opencv_when_tag_is_central_at_far_top_200cm(self):
        # Can't convert jpg image to a ROS image, so converting it to an OpenCV format and then to a ROS image.
        ros_image = self.create_ros_image("tag_central_far_top_200.jpg")
        self.pubNavData.publish(ros_image)
        rospy.loginfo("tag_central_far_top_200")
        self.compare_data((320.0, 343.0), (5.0, 13.0), (self.account_for_focal_change(190),
                                                        self.account_for_focal_change(215)))

    def test_opencv_when_tag_is_central_20cm(self):
        # Can't convert jpg image to a ROS image, so converting it to an OpenCV format and then to a ROS image.
        ros_image = self.create_ros_image("tag_central_20.jpg")
        self.pubNavData.publish(ros_image)
        rospy.loginfo("tag_central_20")
        self.compare_data((171.0, 430.0), (152.0, 239.0), (self.account_for_focal_change(18.0),
                                                           self.account_for_focal_change(22.0)))

    def test_opencv_when_tag_is_central_275cm(self):
        # Can't convert jpg image to a ROS image, so converting it to an OpenCV format and then to a ROS image.
        ros_image = self.create_ros_image("tag_central_275.jpg")
        self.pubNavData.publish(ros_image)
        rospy.loginfo("tag_central_275")
        self.compare_data((318.0, 336.0), (196.0, 202.0), (self.account_for_focal_change(260.0),
                                                           self.account_for_focal_change(290.0)))

        # def test_opencv_when_tag_is_central_at_far_top_100cm(self):
        # Some reason breaking bamboo
        # #Can't convert jpg image to a ROS image, so converting it to an OpenCV format and then to a ROS image.
        # ros_image = self.create_ros_image("tag_central_far_top_100.jpg")
        #     self.pubNavData.publish(ros_image)
        #     rospy.loginfo("tag_central_far_top_100")
        #     self.compare_data((345.0, 382.0), (49.0, 60.0), (95.0, 105.0))

        # def test_opencv_when_tag_is_top_far_left_200cm(self):
        # this test fails
        #     #Can't convert jpg image to a ROS image, so converting it to an OpenCV format and then to a ROS image.
        #     ros_image = self.create_ros_image("tag_top_far_left_200.jpg")
        #     self.pubNavData.publish(ros_image)
        #     rospy.loginfo("tag_top_far_left_200")
        #     self.compare_data((94.0, 109.0), (85.0, 90.0), (190.0, 215.0))

        # def test_opencv_when_tag_is_far_top_left_100cm(self):
        #     #Can't convert jpg image to a ROS image, so converting it to an OpenCV format and then to a ROS image.
        #     # TODO
        #     # pitch is 137
        #     ros_image = self.create_ros_image("tag_top_far_left_100.jpg")
        #     self.pubNavData.publish(ros_image)
        #     rospy.loginfo("tag_top_far_left_100")
        #     self.compare_data((40.0, 70.0), (62.0, 74.0), (95.0, 105.0))

        # def test_opencv_when_tag_is_far_top_left_150cm(self):
        # TODO
        # pitch is 195!
        #     #Can't convert jpg image to a ROS image, so converting it to an OpenCV format and then to a ROS image.
        #     ros_image = self.create_ros_image("tag_top_far_left_150.jpg")
        #     self.pubNavData.publish(ros_image)
        #     rospy.loginfo("tag_top_far_left_150")
        #     self.compare_data((91.0, 111.0), (54.0, 60.0), (145.0, 160.0))


if __name__ == '__main__':
    rostest.rosrun("src", "test_opencv", TestOpenCV)