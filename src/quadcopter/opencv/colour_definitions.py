#!/usr/bin/env python
import numpy

# KEPT FOR TESTS
PRINTED_TAG_GPY = {
    # Green  41, 255, 0
    "L_Left": numpy.array([50, 50, 20], numpy.uint8),
    "U_Left": numpy.array([70, 255, 255], numpy.uint8),
    # Purple 237, 0, 237
    "L_Mid": numpy.array([155, 70, 20], numpy.uint8),
    "U_Mid": numpy.array([170, 255, 255], numpy.uint8),
    # Yellow 255, 255, 0
    "L_Right": numpy.array([25, 90, 50], numpy.uint8),
    "U_Right": numpy.array([40, 255, 255], numpy.uint8)
}

DARWIN_TAG_GPY = {
    # Green
    "L_Left": numpy.array([49, 100, 80], numpy.uint8),
    "U_Left": numpy.array([55, 240, 220], numpy.uint8),
    # Purple [[[172 134 171]]]
    "L_Mid": numpy.array([169, 100, 80], numpy.uint8),
    "U_Mid": numpy.array([175, 240, 220], numpy.uint8),
    # Yellow [[[ 27 170 236]]]
    "L_Right": numpy.array([24, 150, 110], numpy.uint8),
    "U_Right": numpy.array([30, 240, 250], numpy.uint8)
}

DARWIN_TAG_RBO = {
    # Red 5 213 178 [[[  7 188 224]]]
    "L_Left": numpy.array([2, 150, 75], numpy.uint8),
    "U_Left": numpy.array([8, 250, 255], numpy.uint8),
    # Blue 116 155  79 [[[112 142 138]]]
    "L_Mid": numpy.array([109, 130, 50], numpy.uint8),
    "U_Mid": numpy.array([117, 255, 255], numpy.uint8),
    # Orange 18 204 209 [[[ 22 215 249]]]
    "L_Right": numpy.array([19, 180, 100], numpy.uint8),
    "U_Right": numpy.array([25, 255, 255], numpy.uint8)
}

WOOLF_TAG_RBO = {
    # RED   140, 45, 40 / 247, 145, 110 ||| [[[  2 182 140]]] / [[[  8 141 247]]]
    "L_Left": numpy.array([0, 130, 90], numpy.uint8),
    "U_Left": numpy.array([10, 230, 255], numpy.uint8),
    # Blue 99, 132, 192 / 32, 34, 59 ||| [[[109 124 192]]] / [[[118 117  59]]]
    "L_Mid": numpy.array([108, 75, 20], numpy.uint8),
    "U_Mid": numpy.array([120, 200, 200], numpy.uint8),
    # Orange 255, 224, 91 / 168. 106, 34 ||| [[[ 24 164 255]]] / [[[ 16 203 168]]]
    "L_Right": numpy.array([10, 160, 100], numpy.uint8),
    "U_Right": numpy.array([25, 250, 255], numpy.uint8)
}

WOOLF_PINK = {
    # 173 125 192
    # lower old: 160 50 50
    "lower": numpy.array([170, 70, 80], numpy.uint8),
    "upper": numpy.array([180, 255, 255], numpy.uint8),
}

WOOLF_TAG_GPY = {
    # Green 144, 212, 128/ 59, 94, 57 ||| [[[ 54 101 212]]]/[[[ 58 100  94]]]  [[[ 62 115 100]]]
    "L_Left": numpy.array([52, 60, 60], numpy.uint8),
    "U_Left": numpy.array([62, 170, 255], numpy.uint8),
    # Purple 193, 94, 142 / 90, 44, 60 ||| [[[165 131 193]]]/[[[170 130  90]]]
    "L_Mid": numpy.array([165, 70, 60], numpy.uint8),
    "U_Mid": numpy.array([175, 220, 255], numpy.uint8),
    # Yellow 250 249 97 / 160 149 58   |||  [[[ 30 156 250]]] / [[[ 27 163 160]]]
    "L_Right": numpy.array([24, 90, 90], numpy.uint8),
    "U_Right": numpy.array([32, 220, 255], numpy.uint8)
}

SINGLE_PINK = {
    # 173 125 192
    "lower": numpy.array([170, 50, 50], numpy.uint8),
    "upper": numpy.array([175, 255, 255], numpy.uint8),
}

SHED_TAG_GPY = {
    # Green [[[ 48 148 112]]]  [[[ 52 124 179]]]
    "L_Left": numpy.array([40, 100, 80], numpy.uint8),
    "U_Left": numpy.array([55, 255, 255], numpy.uint8),
    # Purple [[[162 146 103]]]
    "L_Mid": numpy.array([159, 100, 80], numpy.uint8),
    "U_Mid": numpy.array([165, 250, 250], numpy.uint8),
    # Yellow [[[ 28 218 179]]]
    "L_Right": numpy.array([26, 150, 110], numpy.uint8),
    "U_Right": numpy.array([30, 255, 255], numpy.uint8)
}

SHED_TAG_RBO = {
    # RED   [[[  4 198 130]]] [[[  5 152 221]]] [[[  6 153 234]]]
    "L_Left": numpy.array([1, 110, 100], numpy.uint8),
    "U_Left": numpy.array([7, 220, 250], numpy.uint8),
    # Blue [[[115 167 134]]] [[[113 145 183]]]
    "L_Mid": numpy.array([110, 110, 50], numpy.uint8),
    "U_Mid": numpy.array([120, 255, 255], numpy.uint8),
    # Orange [[[ 21 221 219]]] [[[ 24 187 255]]]
    "L_Right": numpy.array([20, 170, 100], numpy.uint8),
    "U_Right": numpy.array([30, 255, 255], numpy.uint8)
}

COLOUR_SETS = {
    "Printed_Tag_GPY": PRINTED_TAG_GPY,
    "Darwin_Tag_GPY": DARWIN_TAG_GPY,
    "Darwin_Tag_RBO": DARWIN_TAG_RBO,
    "Woolf_pink": WOOLF_PINK,
    "Woolf_Tag_RBO": WOOLF_TAG_RBO,
    "Woolf_Tag_GPY": WOOLF_TAG_GPY,
    "Shed_Tag_GPY": SHED_TAG_GPY,
    "Shed_Tag_RBO": SHED_TAG_RBO,
    "pink": SINGLE_PINK
}