#!/usr/bin/env python
import math

import rospy


FOCAL_LENGTH = 450.0
WIDTH_OF_TAG = 9.3, 41.0

# Check if 2 values are within a margin of error
def approx_equal(value1, value2, error_margin):
    return value2 - error_margin <= value1 <= value2 + error_margin


# Check if two boxes next to each other
def are_adjacent(left_box, right_box):
    (left_box_x, left_box_y), (width_l_1, width_l_2), _ = left_box
    (right_box_x, right_box_y), (width_r_1, width_r_2), _ = right_box

    width_l = max(width_l_1, width_l_2)
    width_r = max(width_r_1, width_r_2)

    width_avg = (width_l + width_r) / 2.0

    diff_x = right_box_x - left_box_x
    diff_y = right_box_y - left_box_y

    diff_distance = math.sqrt(math.pow(diff_x, 2) + math.pow(diff_y, 2))

    return approx_equal(diff_distance, width_avg, max(width_avg * 0.2, 5))


def get_area(box):
    _, (width, height), _ = box
    return width * height


# Interpolate approximate centre of tag from 2 outer boxes
def get_relative_point(left_box, right_box):
    ((right_box_x, right_box_y), _, _) = right_box
    ((left_box_x, left_box_y), _, _) = left_box

    return left_box_x + (0.5 * (right_box_x - left_box_x)), left_box_y + (0.5 * (right_box_y - left_box_y))


# Return the position of a tag
# Allow for 2 erroneous checks before aborting the frame
def get_position(left_box, centre_box, right_box):
    errors = 0
    left_failed = False
    right_failed = False
    centre_failed = False

    if not are_adjacent(left_box, centre_box):
        rospy.logdebug(rospy.get_caller_id() + " left and centre adjacency failed")
        errors += 1
        left_failed = True
    if not are_adjacent(centre_box, right_box):
        rospy.logdebug(rospy.get_caller_id() + " centre and right adjacency failed")
        errors += 1
        right_failed = True

    # If previous adjacency checks both failed, assume the centre box is the problem
    if left_failed and right_failed:
        ((x, y), (height, width), rot) = left_box
        actual_width = max(height, width)
        adjusted_left = ((x + actual_width, y), (height, width), rot)
        # Check side blocks are separated by roughly one width
        if are_adjacent(adjusted_left, right_box):
            centre_failed = True
            errors = 1
            left_failed = False
            right_failed = False

    left_box_area = get_area(left_box)
    centre_box_area = get_area(centre_box)
    right_box_area = get_area(right_box)

    # Set average area to a reliable block
    if not centre_failed:
        average_area = centre_box_area
    elif not left_failed:
        average_area = left_box_area
    elif not right_failed:
        average_area = right_box_area
    else:
        return

    if not approx_equal(left_box_area, average_area, average_area * 0.3):
        rospy.logdebug(rospy.get_caller_id() + " Left box Size Failed")
        if not left_failed:
            errors += 1
            left_failed = True
    if not approx_equal(centre_box_area, average_area, average_area * 0.3):
        rospy.logdebug(rospy.get_caller_id() + " Centre box Size Failed")
        if not centre_failed:
            errors += 1
            centre_failed = True
    if not approx_equal(right_box_area, average_area, average_area * 0.3):
        rospy.logdebug(rospy.get_caller_id() + " Right box Size Failed")
        if not right_failed:
            errors += 1
            right_failed = True

    if errors > 1:
        rospy.logdebug(rospy.get_caller_id() + " Green box: " + str(left_box))
        rospy.logdebug(rospy.get_caller_id() + " Purple box: " + str(centre_box))
        rospy.logdebug(rospy.get_caller_id() + " Yellow box: " + str(right_box))
        rospy.logdebug(rospy.get_caller_id() + "===================================")
        return
    else:
        if not centre_failed:
            (point, _, _) = centre_box
            distance = get_distance(centre_box)
        elif not right_failed and not left_failed:
            # Estimate centre of tag from flanking boxes
            point = get_relative_point(left_box, right_box)
            distance = get_distance(left_box)
        else:
            return

        return point, distance


# Calculate distance of tag based on known focal length and perceived size of a block of colour
def get_distance(box):
    focal_length = rospy.get_param("~focal_length")
    tag_width = rospy.get_param("~tag_width")
    (_, (width, height), _) = box
    pixel_width = max(width, height)

    # known_distance = 100.0
    # # Use this to calculate the focal length
    # focal_length = pixel_width * known_distance / WIDTH_OF_TAG
    # print "Focal length ", focal_length

    distance = tag_width * focal_length / pixel_width
    return distance