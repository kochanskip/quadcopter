#!/usr/bin/env python
import cv2

# Remove parts of the image that are outside the range of colours we have specified
def __generate_masked_image(hsv, lower, upper):
    mask = cv2.inRange(hsv, lower, upper)
    return mask


# Return a box object of the largest matching block of colour
def get_colour_box(hsv, lower, upper):
    mask = __generate_masked_image(hsv, lower, upper)

    contours, hierarchy = cv2.findContours(mask, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

    max_area = 0
    largest_contour = None
    for idx, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if area > max_area:
            max_area = area
            largest_contour = contour

    if largest_contour is not None:
        return cv2.minAreaRect(largest_contour)

    return