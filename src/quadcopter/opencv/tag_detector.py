#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from std_msgs.msg import Float32

import cv2
from quadcopter.opencv.colour_definitions import COLOUR_SETS
from quadcopter.opencv.image_filter import get_colour_box
from quadcopter.opencv.position_calculator import get_position, get_distance


# Takes incoming frames and returns tag location and distance
class TagDetector:
    def __init__(self):
        rospy.init_node('opencv')

        self.bridge = CvBridge()

        self.numberOfFrames = 1
        self.passes = 0
        self.fails = 0
        self.count = 0

        self.ns = rospy.get_namespace()
        self.image_sub = rospy.Subscriber(self.ns + "quadcopter/image/image_rect_color", Image, self.read_image)
        self.pubRoll = rospy.Publisher(self.ns + "quadcopter/tag_data/roll", Float32, queue_size=1)
        self.pubHeight = rospy.Publisher(self.ns + "quadcopter/tag_data/height", Float32, queue_size=1)
        self.pubPitch = rospy.Publisher(self.ns + "quadcopter/tag_data/pitch", Float32, queue_size=1)

        self.colours = rospy.get_param("~colour_set")
        self.colourSet = COLOUR_SETS[self.colours]

        rospy.spin()

    def publish_missing(self):
        rospy.loginfo(rospy.get_caller_id() + "MISSING")
        self.pubRoll.publish(Float32(-1.0))
        self.pubHeight.publish(Float32(-1.0))
        self.pubPitch.publish(Float32(-1.0))

    def publish_found(self, point):
        rospy.loginfo(rospy.get_caller_id() + str(point))
        (roll, height), distance = point
        self.pubRoll.publish(Float32(roll))
        self.pubHeight.publish(Float32(height))
        self.pubPitch.publish(Float32(distance))

    def read_image(self, data):
        try:
            self.colours = rospy.get_param("~colour_set")
            self.colourSet = COLOUR_SETS[self.colours]

            # Converts raw images to HSV
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
            hsv = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)

            # Check if we are using a single colour tag
            if "pink" in self.colours:
                tag = get_colour_box(hsv, self.colourSet['lower'], self.colourSet['upper'])
                if tag is not None:
                    distance = get_distance(tag)
                    # Set max distance of pink tag to 5m
                    # This is to ensure small amounts of noise do not get reported a tag.
                    if distance < 500:
                        (roll, height), _, _ = tag
                        self.publish_found(((roll, height), distance))
                    else:
                        self.publish_missing()
                else:
                    self.publish_missing()
            else:
                right_box = get_colour_box(hsv, self.colourSet['L_Right'], self.colourSet['U_Right'])
                middle_box = get_colour_box(hsv, self.colourSet['L_Mid'], self.colourSet['U_Mid'])
                left_box = get_colour_box(hsv, self.colourSet['L_Left'], self.colourSet['U_Left'])

                if right_box and left_box and middle_box:
                    point = get_position(left_box, middle_box, right_box)
                    if point is not None:
                        self.publish_found(point)
                    else:
                        self.publish_missing()
                else:
                    self.publish_missing()

        except CvBridgeError, e:
            print e


if __name__ == '__main__':
    TagDetector()
