#!/usr/bin/env python
from ardrone_autonomy.msg import Navdata
import rospy
from std_msgs.msg import Float32

# Used create split navdata into separate topics for PIDs driven by onboard tag detection.
class NavDataSplitter(object):
    def __init__(self):
        rospy.init_node('NavDataSplitter')

        self.__navData = None

        self.ns = rospy.get_namespace()
        self.subNavData = rospy.Subscriber(self.ns + 'ardrone/navdata', Navdata, self.receive_navdata)
        # leftright
        self.pubRollPID = rospy.Publisher(self.ns + 'quadcopter/tag_data/roll', Float32, queue_size=1)
        # fwd/back
        self.pubPitchPID = rospy.Publisher(self.ns + 'quadcopter/tag_data/pitch', Float32, queue_size=1)
        # up/down
        self.pubHeightPID = rospy.Publisher(self.ns + 'quadcopter/tag_data/height', Float32, queue_size=1)

        rospy.spin()

    def receive_navdata(self, navdata):
        self.__navData = navdata
        if navdata.tags_count:
            # Extract the values from the list
            tags_xc = float(navdata.tags_xc[0])
            tags_yc = float(navdata.tags_yc[0])
            tags_distance = navdata.tags_distance[0]

            self.pubRollPID.publish(Float32(tags_xc))
            self.pubHeightPID.publish(Float32(tags_yc))
            self.pubPitchPID.publish(Float32(tags_distance))
        else:
            error_value = Float32(-1.0)
            self.pubRollPID.publish(error_value)
            self.pubHeightPID.publish(error_value)
            self.pubPitchPID.publish(error_value)


if __name__ == '__main__':
    try:
        NavDataSplitter()
    except rospy.ROSInterruptException, e:
        rospy.logfatal(rospy.get_caller_id() + ' ' + e.message)
