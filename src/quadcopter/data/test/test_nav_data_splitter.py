#!/usr/bin/env python
import unittest
import time

from ardrone_autonomy.msg import Navdata
import rospy
import rostest
from std_msgs.msg import Float32


class TestNavDataSplitter(unittest.TestCase):
    # For Test data in this...

    # xc MUST be an integer list
    # yc MUST be an integer list
    # distance MUST be a float list

    # ANY OF THE VALUES WILL ONLY EVER BE BLANK WHEN THE TAG IS NOT VISIBLE

    # These are the types of values that are ACTUALLY in the navdata, so need to be there in this EXACT format.

    def setUp(self):
        rospy.init_node('TestNavDataSplitter')
        self.ns = rospy.get_namespace()
        self.pubNavData = rospy.Publisher(self.ns + 'ardrone/navdata', Navdata, queue_size=1)
        self.subPIDDataRoll = rospy.Subscriber(self.ns + "quadcopter/tag_data/roll", Float32, self.receive_data_roll)
        self.subPIDDataHeight = rospy.Subscriber(self.ns + "quadcopter/tag_data/height", Float32,
                                                 self.receive_data_height)
        self.subPIDDataPitch = rospy.Subscriber(self.ns + "quadcopter/tag_data/pitch", Float32, self.receive_data_pitch)

        self.navdata = Navdata()
        self.receivedCommandRoll = 0
        self.receivedCommandHeight = 0
        self.receivedCommandPitch = 0
        self.receivedRoll = False
        self.receivedHeight = False
        self.receivedPitch = False

        # Allowing set up time for the multiplexer
        rospy.sleep(0.5)

    def receive_data_roll(self, data):
        self.receivedCommandRoll = data
        self.receivedRoll = True

    def receive_data_height(self, data):
        self.receivedCommandHeight = data
        self.receivedHeight = True

    def receive_data_pitch(self, data):
        self.receivedCommandPitch = data
        self.receivedPitch = True

    def test_when_roll_is_neutral_and_height_and_pitch_at_minimum_then_separate_correctly(self):
        self.navdata.tags_count = 1
        self.navdata.tags_xc = [500]  # roll
        self.navdata.tags_yc = [0]  # height
        self.navdata.tags_distance = [0.0]  # pitch

        rospy.loginfo(self.navdata)
        self.pubNavData.publish(self.navdata)
        start_time = time.time()
        while (not (self.receivedRoll and self.receivedHeight and self.receivedPitch)) \
                and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.receivedRoll and self.receivedHeight and self.receivedPitch:
            self.receivedRoll = False
            self.receivedHeight = False
            self.receivedPitch = False
            self.assertEqual((self.receivedCommandRoll, self.receivedCommandHeight, self.receivedCommandPitch),
                             (Float32(500.0), Float32(0.0), Float32(0.0)))
        else:
            self.assertTrue(False, msg="Never received Roll data from the navdata splitter, timed out.")

    def test_when_height_is_neutral_and_roll_and_pitch_at_minimum_then_separate_correctly(self):
        self.navdata.tags_count = 1
        self.navdata.tags_xc = [0]  # roll
        self.navdata.tags_yc = [500]  # height
        self.navdata.tags_distance = [0.0]  # pitch

        self.pubNavData.publish(self.navdata)
        start_time = time.time()
        while (not (self.receivedRoll and self.receivedHeight and self.receivedPitch)) \
                and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.receivedRoll and self.receivedHeight and self.receivedPitch:
            self.receivedRoll = False
            self.receivedHeight = False
            self.receivedPitch = False
            self.assertEqual((self.receivedCommandRoll, self.receivedCommandHeight, self.receivedCommandPitch),
                             (Float32(0.0), Float32(500.0), Float32(0.0)))
        else:
            self.assertTrue(False, msg="Never received Pitch data from the navdata splitter, timed out.")

    def test_when_pitch_is_neutral_and_height_and_roll_at_minimum_then_separate_correctly(self):
        self.navdata.tags_count = 1
        self.navdata.tags_xc = [0]  # roll
        self.navdata.tags_yc = [0]  # height
        self.navdata.tags_distance = [100.0]  # pitch

        self.pubNavData.publish(self.navdata)
        start_time = time.time()
        while (not (self.receivedRoll and self.receivedHeight and self.receivedPitch)) \
                and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.receivedRoll and self.receivedHeight and self.receivedPitch:
            self.receivedRoll = False
            self.receivedHeight = False
            self.receivedPitch = False
            self.assertEqual((self.receivedCommandRoll, self.receivedCommandHeight, self.receivedCommandPitch),
                             (Float32(0.0), Float32(0.0), Float32(100.0)))
        else:
            self.assertTrue(False, msg="Never received Height data from the navdata splitter, timed out.")

    def test_when_roll_and_height_and_pitch_are_neutral_then_separate_correctly(self):
        self.navdata.tags_count = 1
        self.navdata.tags_xc = [500]  # roll
        self.navdata.tags_yc = [500]  # height
        self.navdata.tags_distance = [100.0]  # pitch

        self.pubNavData.publish(self.navdata)
        start_time = time.time()
        while (not (self.receivedRoll and self.receivedHeight and self.receivedPitch)) \
                and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.receivedRoll and self.receivedHeight and self.receivedPitch:
            self.receivedRoll = False
            self.receivedHeight = False
            self.receivedPitch = False
            self.assertEqual((self.receivedCommandRoll, self.receivedCommandHeight, self.receivedCommandPitch),
                             (Float32(500.0), Float32(500.0), Float32(100.0)))
        else:
            self.assertTrue(False, msg="Never received all data from the navdata splitter, timed out.")

    def test_when_roll_and_height_and_pitch_are_empty_then_receive_minus_one_directions(self):
        self.navdata.tags_count = 0
        self.navdata.tags_yc = []  # roll
        self.navdata.tags_xc = []  # height
        self.navdata.tags_distance = []  # pitch

        self.pubNavData.publish(self.navdata)
        start_time = time.time()
        while (not (self.receivedRoll and self.receivedHeight and self.receivedPitch)) \
                and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.receivedRoll and self.receivedHeight and self.receivedPitch:
            self.receivedRoll = False
            self.receivedHeight = False
            self.receivedPitch = False
            self.assertEqual((self.receivedCommandRoll, self.receivedCommandHeight, self.receivedCommandPitch),
                             (Float32(-1.0), Float32(-1.0), Float32(-1.0)))
        else:
            self.assertTrue(False, msg="Never received all data from the navdata splitter, timed out.")


if __name__ == '__main__':
    rostest.rosrun("src", "test_nav_data_splitter", TestNavDataSplitter)