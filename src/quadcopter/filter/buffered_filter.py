#!/usr/bin/env python
import rospy
from std_msgs.msg import Float32
from std_srvs.srv import Empty, EmptyResponse

# Basic averaging filter for smoothing incoming data
class BufferedFilter(object):
    def __init__(self):
        rospy.init_node('BufferedFilter')
        self.ns = rospy.get_namespace()
        self.pubFilteredData = rospy.Publisher(self.ns + rospy.get_param("~pub_name"), Float32, queue_size=1)
        self.subTagData = rospy.Subscriber(self.ns + rospy.get_param("~sub_name"), Float32, self.filter)
        self.range = rospy.get_param("~range")
        self.bufferSize = rospy.get_param("~bufferSize")

        self.buff_list = []

        rospy.Service(self.ns + rospy.get_param("~reset_srv"), Empty, self.reset)
        rospy.spin()

    # Publishes the average of the configured number of previous data points
    def filter(self, value):
        if value.data == -1.0:
            self.pubFilteredData.publish(value)
        elif len(self.buff_list) >= self.bufferSize:
            self.buff_list.pop(0)
            self.buff_list.append(value.data)
            self.pubFilteredData.publish(Float32(sum(self.buff_list) / len(self.buff_list)))
        else:
            self.buff_list.append(value.data)
            self.pubFilteredData.publish(Float32(sum(self.buff_list) / len(self.buff_list)))

    # Reject outlying values
    def within_range(self, buff, value):
        last_average = sum(buff) / len(buff)
        if (last_average - (self.range * 0.2)) < value < ((self.range * 0.2) + last_average):
            return True
        else:
            return False

    def reset(self, data):
        rospy.loginfo(rospy.get_caller_id() + " Resetting Buffered Filter.")
        self.buff_list = []
        return EmptyResponse()


if __name__ == '__main__':
    BufferedFilter()
