#!/usr/bin/env python
import unittest
import time

import rospy
import rostest
from std_msgs.msg import Float32
from std_srvs.srv import Empty as srv_Empty


class TestBufferedFilter(unittest.TestCase):
    def setUp(self):
        rospy.init_node('TestBufferedFilter')

        self.pubTagData = rospy.Publisher("/quadcopter/tag_data/roll", Float32, queue_size=1)
        self.subPIDData = rospy.Subscriber("/quadcopter/filter/roll", Float32, self.receive_data)

        rospy.wait_for_service('/quadcopter/filter/reset_roll')
        self.reset = rospy.ServiceProxy('/quadcopter/filter/reset_roll', srv_Empty)

        self.rollTagData = 0.0
        self.received_data = 0.0

        self.received = False

        # Allowing set up time for the filter
        rospy.sleep(0.5)

    def receive_data(self, sub_data):
        self.received_data = sub_data.data
        self.received += 1

    def test_reset_clears_buffer(self):
        self.received = 0
        self.reset()
        self.pubTagData.publish(Float32(300.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(250.0))
        rospy.sleep(0.1)
        self.reset()
        self.pubTagData.publish(Float32(300.0))
        start_time = time.time()
        while not self.received == 3 and not time.time() > start_time + 3:
            rospy.sleep(0.1)

        self.assertEqual(self.received_data, 300.0)

    def test_no_tag_returns_no_tag(self):
        self.received = 0
        self.reset()
        self.pubTagData.publish(Float32(-1.0))
        start_time = time.time()
        while not self.received == 3 and not time.time() > start_time + 3:
            rospy.sleep(0.1)

        self.assertEqual(self.received_data, -1.0)

    def test_single_publish_returns_same_value_published(self):
        self.received = 0
        self.reset()
        self.pubTagData.publish(Float32(300.0))
        start_time = time.time()
        while not self.received == 1 and not time.time() > start_time + 3:
            rospy.sleep(0.1)

        self.assertEqual(self.received_data, 300.0)

    def test_3_values_return_their_average(self):
        self.received = 0
        self.reset()
        self.pubTagData.publish(Float32(300.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(250.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(200.0))
        rospy.sleep(0.1)
        start_time = time.time()
        rospy.sleep(2)
        while not self.received == 3 and not time.time() > start_time + 3:
            rospy.sleep(0.1)

        self.assertEqual(self.received_data, 250.0)

    def test_full_buffer_values_return_their_average(self):
        self.received = 0
        self.reset()
        self.pubTagData.publish(Float32(300.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(250.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(200.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(150.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(100.0))
        start_time = time.time()
        while not self.received == 5 and not time.time() > start_time + 3:
            rospy.sleep(0.1)

        self.assertEqual(self.received_data, 200.0)

    def test_single_missing_frame_returns_average(self):
        self.received = 0
        self.reset()
        self.pubTagData.publish(Float32(300.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(250.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(200.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(-1.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(150.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(100.0))
        start_time = time.time()
        while not self.received == 6 and not time.time() > start_time + 3:
            rospy.sleep(0.1)

        self.assertEqual(self.received_data, 200.0)

    def test_last_missing_frame_returns_minus_1(self):
        self.received = 0
        self.reset()
        self.pubTagData.publish(Float32(300.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(250.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(200.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(150.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(100.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(-1.0))
        start_time = time.time()
        while not self.received == 6 and not time.time() > start_time + 3:
            rospy.sleep(0.1)

        self.assertEqual(self.received_data, -1.0)

    def test_always_average_of_last_five(self):
        self.received = 0
        self.reset()
        self.pubTagData.publish(Float32(300.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(250.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(200.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(150.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(100.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(150.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(200.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(250.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(300.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(350.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(400.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(450.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(500.0))
        rospy.sleep(0.1)
        self.pubTagData.publish(Float32(550.0))
        start_time = time.time()
        while not self.received == 14 and not time.time() > start_time + 3:
            rospy.sleep(0.1)

        self.assertEqual(self.received_data, 450.0)


if __name__ == '__main__':
    rostest.rosrun("src", "test_pid_controller", TestBufferedFilter)