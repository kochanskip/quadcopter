#!/usr/bin/env python
from ardrone_autonomy.msg import Navdata
from geometry_msgs.msg import Twist
import rospy
from std_msgs.msg import Empty
from std_srvs.srv import Empty as srv_Empty

from quadcopter.drone_states import DroneStates

# Drone commands are sent via this class to allow for filtering of commands depending on stage of flight
class DroneControl(object):
    def __init__(self):
        rospy.init_node('DroneControl')
        self.state = -1
        self.override = False
        self.setup = False
        self.follower = False

        self.ns = rospy.get_namespace()
        self.follower = rospy.get_param('~follower')

        self.subMoveCommand = rospy.Subscriber(self.ns + 'quadcopter/commands/move', Twist, self.set_move_command)
        self.subOverrideCommand = rospy.Subscriber(self.ns + 'quadcopter/commands/override', Twist,
                                                   self.set_override_command)
        self.subAllFlatTrim = rospy.Subscriber('/quadcopter/commands/all/flattrim', Empty, self.receive_flattrim)
        self.subAllLand = rospy.Subscriber('/quadcopter/commands/all/land', Empty, self.receive_land)
        self.subAllTakeOff = rospy.Subscriber('/quadcopter/commands/all/takeoff', Empty, self.receive_takeoff)
        self.subAllReset = rospy.Subscriber('/quadcopter/commands/all/reset', Empty, self.receive_reset)
        self.subAllOverride = rospy.Subscriber('/quadcopter/commands/all/override', Twist, self.set_override_command)
        self.subAllPidReset = rospy.Subscriber('/quadcopter/commands/all/reset_pids', Empty, self.reset_pids)
        self.subResetAllOverride = rospy.Subscriber('/quadcopter/commands/all/resetoverride', Empty,
                                                    self.reset_override)
        self.subNavData = rospy.Subscriber(self.ns + 'ardrone/navdata', Navdata, self.receive_navdata)
        self.subResetOverride = rospy.Subscriber(self.ns + 'quadcopter/commands/resetoverride', Empty,
                                                 self.reset_override)

        self.subAllResetYawSetPoint = rospy.Subscriber('/quadcopter/commands/all/reset_set_point', Empty,
                                                       self.receive_reset_yaw_set_point)

        self.pubMoveCommand = rospy.Publisher(self.ns + 'cmd_vel', Twist, queue_size=1)
        self.pubLand = rospy.Publisher(self.ns + 'ardrone/land', Empty, queue_size=1)
        self.pubTakeOff = rospy.Publisher(self.ns + 'ardrone/takeoff', Empty, queue_size=1)
        self.pubReset = rospy.Publisher(self.ns + 'ardrone/reset', Empty, queue_size=1)
        self.pubResetYawSetPoint = rospy.Publisher(self.ns + 'quadcopter/pid/reset_yaw_set_point', Empty, queue_size=1)

        rospy.spin()

    def receive_land(self, event):
        self.pubLand.publish(event)

    def receive_takeoff(self, event):
        self.pubTakeOff.publish(event)

    def receive_reset(self, event):
        self.pubReset.publish(event)

    def receive_navdata(self, navdata):
        self.state = navdata.state

        # Once the drone has reached hover, we consider the system setup
        if not self.setup and (self.state == DroneStates.Flying or self.state == DroneStates.Hovering):
            if self.follower:
                self.reset_pids(None)
            self.setup = True

        # Set back to initial state once we have landed
        if self.state == DroneStates.Landed:
            self.override = False
            self.setup = False

    # Only when we are fully setup and there is no control override do we forward commands
    def set_move_command(self, command):
        if not self.override:
            if self.state == DroneStates.Flying or self.state == DroneStates.Hovering and self.setup:
                self.pubMoveCommand.publish(command)

    # Allows all other control to be overridden in certain situations
    def set_override_command(self, command):
        self.override = True
        if self.state == DroneStates.Flying or self.state == DroneStates.Hovering and self.setup:
            self.pubMoveCommand.publish(command)

    def reset_override(self, _):
        rospy.loginfo(rospy.get_caller_id() + " Drone Control Override reset")
        if self.state != DroneStates.Flying or self.state != DroneStates.Hovering and self.setup:
            self.override = False

    def receive_flattrim(self, _):
        rospy.loginfo(rospy.get_caller_id() + " Flattrim Called")
        rospy.ServiceProxy(self.ns + 'ardrone/flattrim', srv_Empty).call()

    def receive_reset_yaw_set_point(self, _):
        rospy.wait_for_service(self.ns + 'quadcopter/pid/reset_yaw_set_point')
        rospy.ServiceProxy(self.ns + 'quadcopter/pid/reset_yaw_set_point', srv_Empty).call()

    def reset_pids(self, _):
        rospy.wait_for_service(self.ns + 'quadcopter/pid/reset_roll')
        rospy.wait_for_service(self.ns + 'quadcopter/pid/reset_pitch')
        rospy.wait_for_service(self.ns + 'quadcopter/pid/reset_height')
        rospy.wait_for_service(self.ns + 'quadcopter/pid/reset_yaw')
        rospy.ServiceProxy(self.ns + 'quadcopter/pid/reset_roll', srv_Empty).call()
        rospy.ServiceProxy(self.ns + 'quadcopter/pid/reset_pitch', srv_Empty).call()
        rospy.ServiceProxy(self.ns + 'quadcopter/pid/reset_height', srv_Empty).call()
        rospy.ServiceProxy(self.ns + 'quadcopter/pid/reset_yaw', srv_Empty).call()
        rospy.loginfo(rospy.get_caller_id() + " PIDs Reset")


if __name__ == '__main__':
    try:
        DroneControl()
    except rospy.ROSInterruptException, e:
        rospy.logfatal(rospy.get_caller_id() + ' ' + e.message)
