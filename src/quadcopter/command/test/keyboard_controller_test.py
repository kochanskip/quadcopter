#!/usr/bin/env python
import unittest
import time

import rospy
from std_msgs.msg import Empty
from std_srvs.srv import Empty as srv_Empty


PKG = 'src'
NAME = 'keyboard_control'


class KeyboardControlTest(unittest.TestCase):
    landReceived = False
    resetReceived = False
    takeoffReceived = False
    allLandReceived = False
    allResetReceived = False
    allTakeoffReceived = False
    flatTrimReceived = False
    resetDroneControlReceived = False
    resetAllDroneControlReceived = False

    def setUp(self):
        rospy.init_node(NAME)
        self.ns = rospy.get_namespace()
        self.subSingleLand = rospy.Subscriber(self.ns + 'ardrone/land', Empty, self.receive_land)
        self.subSingleReset = rospy.Subscriber(self.ns + 'ardrone/reset', Empty, self.receive_reset)
        self.subSingleTakeOff = rospy.Subscriber(self.ns + 'ardrone/takeoff', Empty, self.receive_takeoff)
        self.subAllLand = rospy.Subscriber('/quadcopter/commands/all/land', Empty, self.receive_all_land)
        self.subAllReset = rospy.Subscriber('/quadcopter/commands/all/reset', Empty, self.receive_all_reset)
        self.subAllTakeOff = rospy.Subscriber('/quadcopter/commands/all/takeoff', Empty, self.receive_all_takeoff)

        rospy.Service(self.ns + 'ardrone/flattrim', srv_Empty, self.receive_flattrim)

        rospy.wait_for_service(self.ns + 'quadcopter/test/spacebar')
        rospy.wait_for_service(self.ns + 'quadcopter/test/escape')
        rospy.wait_for_service(self.ns + 'quadcopter/test/t')
        rospy.wait_for_service(self.ns + 'quadcopter/test/l')
        rospy.wait_for_service(self.ns + 'quadcopter/test/f')
        rospy.wait_for_service(self.ns + 'quadcopter/test/r')
        rospy.wait_for_service(self.ns + 'quadcopter/test/e')
        rospy.wait_for_service(self.ns + 'quadcopter/test/backspace')
        rospy.wait_for_service(self.ns + 'quadcopter/test/y')

        self.send_spacebar = rospy.ServiceProxy(self.ns + 'quadcopter/test/spacebar', srv_Empty)
        self.send_escape = rospy.ServiceProxy(self.ns + 'quadcopter/test/escape', srv_Empty)
        self.send_t = rospy.ServiceProxy(self.ns + 'quadcopter/test/t', srv_Empty)
        self.send_l = rospy.ServiceProxy(self.ns + 'quadcopter/test/l', srv_Empty)
        self.send_backspace = rospy.ServiceProxy(self.ns + 'quadcopter/test/backspace', srv_Empty)
        self.send_y = rospy.ServiceProxy(self.ns + 'quadcopter/test/y', srv_Empty)
        self.send_f = rospy.ServiceProxy(self.ns + 'quadcopter/test/f', srv_Empty)
        self.send_r = rospy.ServiceProxy(self.ns + 'quadcopter/test/r', srv_Empty)
        self.send_e = rospy.ServiceProxy(self.ns + 'quadcopter/test/e', srv_Empty)

    def receive_land(self, event):
        self.landReceived = True

    def receive_reset(self, event):
        self.resetReceived = True

    def receive_takeoff(self, event):
        self.takeoffReceived = True

    def receive_all_land(self, event):
        self.allLandReceived = True

    def receive_all_reset(self, event):
        self.allResetReceived = True

    def receive_all_takeoff(self, event):
        self.allTakeoffReceived = True

    def receive_flattrim(self, event):
        self.flatTrimReceived = True

    def receive_reset_drone_control(self, event):
        self.resetDroneControlReceived = True

    def receive_reset_all_drone_control(self, event):
        self.resetAllDroneControlReceived = True

    def test_space_sends_land_to_all(self):
        self.send_spacebar()
        ti = time.time()
        while not self.allLandReceived and not time.time() > ti + 3:
            time.sleep(0.5)
        self.assertTrue(self.allLandReceived)

    def test_escape_sends_reset_to_all(self):
        ti = time.time()
        self.send_escape()
        while not self.allResetReceived and not time.time() > ti + 3:
            time.sleep(0.5)
        self.assertTrue(self.allResetReceived)

    def test_t_sends_takeoff_to_one(self):
        self.send_t()
        ti = time.time()
        while not self.takeoffReceived and not time.time() > ti + 3:
            time.sleep(0.5)
        self.assertTrue(self.takeoffReceived)

    def test_y_sends_takeoff_to_all(self):
        self.send_y()
        ti = time.time()
        while not self.allTakeoffReceived and not time.time() > ti + 3:
            time.sleep(0.5)
        self.assertTrue(self.allTakeoffReceived)

    def test_backspace_sends_reset_to_one(self):
        self.send_backspace()
        ti = time.time()
        while not self.resetReceived and not time.time() > ti + 3:
            time.sleep(0.5)
        self.assertTrue(self.resetReceived)

    def test_l_sends_land_to_one(self):
        self.send_l()
        ti = time.time()
        while not self.landReceived and not time.time() > ti + 3:
            time.sleep(0.5)
        self.assertTrue(self.landReceived)

    def test_f_sends_flat_trim_to_one(self):
        self.assertTrue(True)

    def test_r_sends_reset_drone_control_to_one(self):
        self.send_r()
        ti = time.time()
        while not self.resetDroneControlReceived and not time.time() > ti + 3:
            time.sleep(0.5)
        self.assertTrue(self.resetDroneControlReceived)

    def test_e_sends_reset_drone_control_to_all(self):
        self.send_e()
        ti = time.time()
        while not self.resetAllDroneControlReceived and not time.time() > ti + 3:
            time.sleep(0.5)
        self.assertTrue(self.resetAllDroneControlReceived)


if __name__ == '__main__':
    import rostest

    rostest.rosrun(PKG, NAME, KeyboardControlTest)