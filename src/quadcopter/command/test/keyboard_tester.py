#!/usr/bin/env python
from PySide import QtGui, QtCore
from PySide.QtTest import QTest
import rospy
from std_srvs.srv import Empty, EmptyResponse

from quadcopter.command.keyboard_control import KeyboardControl


# noinspection PyUnusedLocal
class KeyboardTest(KeyboardControl):
    def __init__(self):
        super(KeyboardTest, self).__init__()
        self.send_key_service()

    @staticmethod
    def send_space(data):
        rospy.loginfo(rospy.get_caller_id() + " Sending SPACEBAR")
        QTest.keyClick(display.video_feed, ' ')
        return EmptyResponse()

    @staticmethod
    def send_escape(data):
        rospy.loginfo(rospy.get_caller_id() + " Sending ESCAPE")
        QTest.keyClick(display.video_feed, QtCore.Qt.Key.Key_Escape)
        return EmptyResponse()

    @staticmethod
    def send_t(data):
        rospy.loginfo(rospy.get_caller_id() + " Sending T")
        QTest.keyClick(display.video_feed, QtCore.Qt.Key.Key_T)
        return EmptyResponse()

    @staticmethod
    def send_l(data):
        rospy.loginfo(rospy.get_caller_id() + " Sending L")
        QTest.keyClick(display.video_feed, QtCore.Qt.Key.Key_L)
        return EmptyResponse()

    @staticmethod
    def send_backspace(data):
        rospy.loginfo(rospy.get_caller_id() + " Sending BACKSPACE")
        QTest.keyClick(display.video_feed, QtCore.Qt.Key.Key_Backspace)
        return EmptyResponse()

    @staticmethod
    def send_y(data):
        rospy.loginfo(rospy.get_caller_id() + " Sending Y")
        QTest.keyClick(display.video_feed, QtCore.Qt.Key.Key_Y)
        return EmptyResponse()

    @staticmethod
    def send_f(data):
        rospy.loginfo(rospy.get_caller_id() + " Sending F")
        QTest.keyClick(display.video_feed, QtCore.Qt.Key.Key_F)
        return EmptyResponse()

    @staticmethod
    def send_r(data):
        rospy.loginfo(rospy.get_caller_id() + " Sending R")
        QTest.keyClick(display.video_feed, QtCore.Qt.Key.Key_R)
        return EmptyResponse()

    @staticmethod
    def send_e(data):
        rospy.loginfo(rospy.get_caller_id() + " Sending E")
        QTest.keyClick(display.video_feed, QtCore.Qt.Key.Key_E)
        return EmptyResponse()

    def send_key_service(self):
        self.ns = rospy.get_namespace()
        rospy.Service(self.ns + 'quadcopter/test/spacebar', Empty, self.send_space)
        rospy.Service(self.ns + 'quadcopter/test/escape', Empty, self.send_escape)
        rospy.Service(self.ns + 'quadcopter/test/t', Empty, self.send_t)
        rospy.Service(self.ns + 'quadcopter/test/l', Empty, self.send_l)
        rospy.Service(self.ns + 'quadcopter/test/f', Empty, self.send_f)
        rospy.Service(self.ns + 'quadcopter/test/r', Empty, self.send_r)
        rospy.Service(self.ns + 'quadcopter/test/e', Empty, self.send_e)
        rospy.Service(self.ns + 'quadcopter/test/backspace', Empty, self.send_backspace)
        rospy.Service(self.ns + 'quadcopter/test/y', Empty, self.send_y)
        rospy.loginfo(rospy.get_caller_id() + " Services started")


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication(sys.argv)
    display = KeyboardTest()
    display.show()
    status = app.exec_()
    rospy.signal_shutdown('Keyboard Controller closed')
    sys.exit(status)