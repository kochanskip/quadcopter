#!/usr/bin/env python
import unittest
import time

from ardrone_autonomy.msg import Navdata
from geometry_msgs.msg import Twist
import rospy
from std_msgs.msg import Empty

from quadcopter.drone_states import DroneStates


PKG = 'src'
NAME = 'drone_control_test'


class DroneControlTest(unittest.TestCase):
    landReceived = False
    resetReceived = False
    takeoffReceived = False
    command = None
    navdata = Navdata()

    def setUp(self):
        rospy.init_node(NAME)
        self.ns = rospy.get_namespace()

        self.subSingleLand = rospy.Subscriber(self.ns + 'ardrone/land', Empty, self.receive_land)
        self.subSingleReset = rospy.Subscriber(self.ns + 'ardrone/reset', Empty, self.receive_reset)
        self.subSingleTakeOff = rospy.Subscriber(self.ns + 'ardrone/takeoff', Empty, self.receive_takeoff)
        self.subCmdVel = rospy.Subscriber(self.ns + 'cmd_vel', Twist, self.receive_cmd)

        self.pubAllLand = rospy.Publisher('/quadcopter/commands/all/land', Empty, queue_size=1)
        self.pubAllTakeOff = rospy.Publisher('/quadcopter/commands/all/takeoff', Empty, queue_size=1)
        self.pubAllReset = rospy.Publisher('/quadcopter/commands/all/reset', Empty, queue_size=1)
        self.pubMoveCommand = rospy.Publisher(self.ns + 'quadcopter/commands/move', Twist, queue_size=1)
        self.pubNavData = rospy.Publisher(self.ns + 'ardrone/navdata', Navdata, queue_size=1)
        self.pubOverride = rospy.Publisher(self.ns + 'quadcopter/commands/override', Twist, queue_size=1)

        rospy.sleep(0.5)

        self.navdata.state = DroneStates.Landed
        self.pubNavData.publish(self.navdata)
        self.command = None

    def receive_cmd(self, event):
        self.command = event

    def receive_land(self, event):
        self.landReceived = True

    def receive_reset(self, event):
        self.resetReceived = True

    def receive_takeoff(self, event):
        self.takeoffReceived = True

    def test_drone_state_flying_move_command_passed_to_cmd_vel(self):
        command = Twist()
        command.linear.x = 0.5
        command.linear.y = 1
        command.linear.z = 0.2
        command.angular.z = 0.5
        navdata = Navdata()
        navdata.state = DroneStates.Flying
        self.pubNavData.publish(navdata)
        rospy.sleep(0.1)
        self.pubMoveCommand.publish(command)
        start_time = time.time()
        while not self.command and not time.time() > start_time + 3:
            rospy.sleep(0.1)
        self.assertEqual(command, self.command)

    def test_drone_state_landed_move_command_not_passed_to_cmd_vel(self):
        command = Twist()
        command.linear.x = 0.5
        command.linear.y = 1
        command.linear.z = 0.2
        command.angular.z = 0.5
        navdata = Navdata()
        navdata.state = DroneStates.Landed
        self.pubNavData.publish(navdata)
        rospy.sleep(0.1)
        self.pubMoveCommand.publish(command)
        start_time = time.time()
        while not self.command and not time.time() > start_time + 3:
            rospy.sleep(0.1)
        self.assertEqual(None, self.command)

    def test_override_sent_move_further_commands_not_passed_to_cmd_vel(self):
        command = Twist()
        command.linear.x = 0.5
        command.linear.y = 1
        command.linear.z = 0.2
        command.angular.z = 0.5
        navdata = Navdata()
        navdata.state = DroneStates.Flying
        self.pubNavData.publish(navdata)
        self.pubOverride.publish(Twist())
        rospy.sleep(0.1)
        self.command = None
        self.pubMoveCommand.publish(command)
        start_time = time.time()
        while not self.command and not time.time() > start_time + 3:
            rospy.sleep(0.1)
        self.assertEqual(None, self.command)

    def test_all_land_sent_sends_land(self):
        self.pubAllLand.publish(Empty())
        start_time = time.time()
        while not self.landReceived and not time.time() > start_time + 3:
            rospy.sleep(0.1)
        self.assertTrue(self.landReceived)

    def test_all_takeoff_sent_sends_takeoff(self):
        self.pubAllTakeOff.publish(Empty())
        start_time = time.time()
        while not self.takeoffReceived and not time.time() > start_time + 3:
            rospy.sleep(0.1)
        self.assertTrue(self.takeoffReceived)

    def test_all_reset_sent_sends_reset(self):
        self.pubAllReset.publish(Empty())
        start_time = time.time()
        while not self.resetReceived and not time.time() > start_time + 3:
            rospy.sleep(0.1)
        self.assertTrue(self.resetReceived)


if __name__ == '__main__':
    import rostest

    rostest.rosrun(PKG, NAME, DroneControlTest)