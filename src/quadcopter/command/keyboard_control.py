#!/usr/bin/env python
from threading import Lock

from PySide import QtGui, QtCore
from ardrone_autonomy.msg import Navdata
from geometry_msgs.msg import Twist
import rospy
from sensor_msgs.msg import Image
from std_msgs.msg import Empty
from std_srvs.srv import Empty as srv_Empty

from quadcopter.drone_states import DroneStates


GUI_UPDATE_PERIOD = 20  # ms

# Displays the video feed and state in a window. Key presses are used to trigger basic functions on the drone.
class KeyboardControl(QtGui.QMainWindow):
    DisconnectedMessage = 'Disconnected'
    UnknownMessage = 'Unknown Status'

    def __init__(self):
        super(KeyboardControl, self).__init__()
        rospy.init_node('KeyboardController')

        self.ns = rospy.get_namespace()
        self.setWindowTitle(self.ns + "Quadcopter Keyboard Controller")
        self.video_feed = QtGui.QLabel(self)
        self.setCentralWidget(self.video_feed)
        self.statusMessage = self.DisconnectedMessage

        self.image = None
        self.imageLock = Lock()

        self.redrawTimer = QtCore.QTimer(self)
        self.redrawTimer.timeout.connect(self.redraw_gui)
        self.redrawTimer.start(GUI_UPDATE_PERIOD)

        self.subVideo = rospy.Subscriber(self.ns + rospy.get_param('~cameraTopic'), Image, self.receive_image)
        self.subNavData = rospy.Subscriber(self.ns + 'ardrone/navdata', Navdata, self.receive_navdata)

        self.pubLand = rospy.Publisher(self.ns + 'ardrone/land', Empty, queue_size=1)
        self.pubTakeOff = rospy.Publisher(self.ns + 'ardrone/takeoff', Empty, queue_size=1)
        self.pubReset = rospy.Publisher(self.ns + 'ardrone/reset', Empty, queue_size=1)
        self.pubOverride = rospy.Publisher(self.ns + 'quadcopter/commands/override', Twist, queue_size=1)
        self.pubResetOverride = rospy.Publisher(self.ns + 'quadcopter/commands/resetoverride', Empty, queue_size=1)

        self.pubAllFlattrim = rospy.Publisher('/quadcopter/commands/all/flattrim', Empty, queue_size=1)
        self.pubAllLand = rospy.Publisher('/quadcopter/commands/all/land', Empty, queue_size=1)
        self.pubAllTakeOff = rospy.Publisher('/quadcopter/commands/all/takeoff', Empty, queue_size=1)
        self.pubAllReset = rospy.Publisher('/quadcopter/commands/all/reset', Empty, queue_size=1)
        self.pubAllOverride = rospy.Publisher('/quadcopter/commands/all/override', Twist, queue_size=1)
        self.pubAllResetOverride = rospy.Publisher('/quadcopter/commands/all/resetoverride', Empty, queue_size=1)
        self.pubAllResetYawSetPoint = rospy.Publisher('/quadcopter/commands/all/reset_set_point', Empty, queue_size=1)
        self.pubAllPidReset = rospy.Publisher('/quadcopter/commands/all/reset_pids', Empty, queue_size=1)

        rospy.wait_for_service(self.ns + 'ardrone/flattrim')

    def keyPressEvent(self, event):
        key = event.key()
        if not event.isAutoRepeat():
            # ALL LAND
            if key == QtCore.Qt.Key.Key_Space:
                self.pubAllOverride.publish(Twist())
                self.pubAllLand.publish(Empty())
                rospy.loginfo(rospy.get_caller_id() + " All Land Sent")
            # SINGLE LAND
            if key == QtCore.Qt.Key.Key_L:
                self.pubOverride.publish(Twist())
                self.pubLand.publish(Empty())
                rospy.loginfo(rospy.get_caller_id() + " Single Land Sent")
            # ALL EMERGENCY
            if key == QtCore.Qt.Key.Key_Escape:
                self.pubAllOverride.publish(Twist())
                self.pubAllReset.publish(Empty())
                rospy.loginfo(rospy.get_caller_id() + " All Emergency Signal Sent")
            # SINGLE EMERGENCY
            if key == QtCore.Qt.Key.Key_Backspace:
                self.pubOverride.publish(Twist())
                self.pubReset.publish(Empty())
                rospy.loginfo(rospy.get_caller_id() + " Single Emergency Signal Sent")
            # SINGLE TAKE-OFF
            if key == QtCore.Qt.Key.Key_T:
                self.pubTakeOff.publish(Empty())
                rospy.loginfo(rospy.get_caller_id() + " Single Take off sent")
            # ALL TAKE-OFF
            if key == QtCore.Qt.Key.Key_Y:
                self.pubAllTakeOff.publish(Empty())
                rospy.loginfo(rospy.get_caller_id() + " All Take off sent")
            # SINGLE FLATTRIM
            if key == QtCore.Qt.Key.Key_F:
                rospy.ServiceProxy(self.ns + 'ardrone/flattrim', srv_Empty).call()
                rospy.loginfo(rospy.get_caller_id() + " Flattrim Set")
            # SINGLE RESET OVERRIDE
            if key == QtCore.Qt.Key.Key_R:
                self.pubResetOverride.publish(Empty())
                rospy.loginfo(rospy.get_caller_id() + " Override Reset sent")
            # RESET ALL OVERRIDE + ALL YAW SET POINT + ALL PID + FLATTRIM
            if key == QtCore.Qt.Key.Key_E:
                self.pubAllResetYawSetPoint.publish(Empty())
                self.pubAllResetOverride.publish(Empty())
                self.pubAllFlattrim.publish(Empty())
                self.pubAllPidReset.publish(Empty())

    def receive_navdata(self, navdata):
        msg = DroneStates.StatusMessages[navdata.state] if navdata.state in DroneStates.StatusMessages \
            else self.UnknownMessage
        self.statusMessage = '{} (Battery: {}%)'.format(msg, int(navdata.batteryPercent))

    def receive_image(self, image):
        self.imageLock.acquire()
        try:
            self.image = image
        finally:
            self.imageLock.release()

    def redraw_gui(self):
        if self.image is not None:
            self.imageLock.acquire()
            try:
                qt_image = QtGui.QPixmap.fromImage(QtGui.QImage(self.image.data,
                                                                self.image.width,
                                                                self.image.height,
                                                                QtGui.QImage.Format_RGB888))
            finally:
                self.imageLock.release()

            self.resize(qt_image.width(), qt_image.height())
            self.video_feed.setPixmap(qt_image)
        self.statusBar().showMessage(self.statusMessage)


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication(sys.argv)
    display = KeyboardControl()
    display.show()
    status = app.exec_()
    rospy.signal_shutdown('Keyboard Controller closed')
    sys.exit(status)