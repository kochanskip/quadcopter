#!/usr/bin/env python
import rospy
from std_msgs.msg import Float32
from std_srvs.srv import Empty, EmptyResponse


# Implementation of a simple PID controller
class PIDController(object):
    def __init__(self):
        rospy.init_node('PIDController')

        self.ns = rospy.get_namespace()
        self.pubPIDData = rospy.Publisher(self.ns + rospy.get_param("~pub_name"), Float32, queue_size=1)
        self.subTagData = rospy.Subscriber(self.ns + rospy.get_param("~sub_name"), Float32,
                                           self.update_values)
        rospy.Service(self.ns + rospy.get_param("~reset_srv"), Empty, self.reset)

        self.set_point = rospy.get_param("~set_point")
        self.range = rospy.get_param("~range")

        self.error = 0.0
        self.integral = 0.0
        self.previous_error = 0.0

        self.p_value = 0.0
        self.i_value = 0.0
        self.d_value = 0.0

        self.pid = 0.0

        # Tuning parameters
        self.kp = rospy.get_param("~kp")  # proportional gain
        self.ki = rospy.get_param("~ki")  # integral gain
        self.kd = rospy.get_param("~kd")  # derivative gain

        self.behaviour = rospy.get_param("~tag_behaviour")
        rospy.spin()

    # Positions greater than the set point, will produce a negative value and hence steer right.
    # Negative values steer left, positive steer right.
    def update_values(self, position):
        self.set_point = rospy.get_param("~set_point")

        if position.data >= 0:
            # Normalise based on the configured range of inputs.
            self.error = self.set_point - (position.data / self.range)
        else:
            # If configured, stops movement on tag loss
            if self.behaviour == "still":
                self.error = 0.0

        self.p_value = self.kp * self.error

        self.integral += self.error
        self.i_value = self.integral * self.ki

        self.d_value = self.kd * (self.error - self.previous_error)
        self.previous_error = self.error

        if (self.p_value + self.i_value + self.d_value) == 0.0:
            self.pid = 0.0
        else:
            self.pid = (self.p_value + self.i_value + self.d_value)
        if self.pid > 1:
            self.pid = 1
        elif self.pid < -1:
            self.pid = -1
        self.pubPIDData.publish(self.pid)

    def reset(self, data):
        rospy.loginfo(rospy.get_caller_id() + " Resetting PID Controller")
        self.error = 0.0
        self.integral = 0.0
        self.previous_error = 0.0

        self.p_value = 0.0
        self.i_value = 0.0
        self.d_value = 0.0

        self.pid = 0.0
        return EmptyResponse()


if __name__ == '__main__':
    try:
        PIDController()
    except rospy.ROSInterruptException, e:
        rospy.logfatal(rospy.get_caller_id() + ' ' + e.message)