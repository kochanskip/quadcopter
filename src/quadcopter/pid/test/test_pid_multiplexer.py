#!/usr/bin/env python
import unittest
import time

from geometry_msgs.msg import Twist
import rospy
import rostest
from std_msgs.msg import Float32


class TestPIDMultiplexer(unittest.TestCase):
    def setUp(self):

        rospy.init_node('TestPIDMultiplexer')
        self.ns = rospy.get_namespace()
        self.subMoveCom = rospy.Subscriber(self.ns + "quadcopter/commands/move", Twist, self.receive_data)

        self.pubRollData = rospy.Publisher(self.ns + "quadcopter/pid/roll", Float32, queue_size=1)
        self.pubHeightData = rospy.Publisher(self.ns + "quadcopter/pid/height", Float32, queue_size=1)
        self.pubPitchData = rospy.Publisher(self.ns + "quadcopter/pid/pitch", Float32, queue_size=1)
        self.pubYawData = rospy.Publisher(self.ns + "quadcopter/pid/yaw", Float32, queue_size=1)

        self.rollMoveData = 0.0
        self.heightMoveData = 0.0
        self.pitchMoveData = 0.0
        self.yawMoveData = 0.0

        self.receivedCommand = Twist()
        self.expectedCommand = Twist()

        self.received = False

        # Allowing set up time for the multiplexer
        rospy.sleep(0.5)

    def receive_data(self, sub_command):
        self.receivedCommand = sub_command
        self.received = True

    def test_1_when_all_input_is_neutral_then_command_directions_are_set_to_neutral(self):
        self.rollMoveData = 0.0
        self.heightMoveData = 0.0
        self.pitchMoveData = 0.0
        self.yawMoveData = 0.0

        self.pubRollData.publish(self.rollMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer X input")
        self.pubHeightData.publish(self.heightMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer Y input")
        self.pubPitchData.publish(self.pitchMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer z input")
        self.pubYawData.publish(self.yawMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer angular z input")

        # roll goes to linear.y
        # height goes to linear.z
        # pitch goes to linear.x
        # yaw goes to angular.z
        self.expectedCommand.linear.x = 0.0
        self.expectedCommand.linear.y = 0.0
        self.expectedCommand.linear.z = 0.0
        self.expectedCommand.angular.z = 0.0

        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 2)
            self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 2)
            self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 2)
            self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 2)
            self.assertEqual(self.receivedCommand, self.expectedCommand)
        else:
            self.assertTrue(False, msg="Never received data from the PID Multiplexer, timed out.")

    def test_2_when_all_inputs_are_at_max_positive_then_command_directions_are_at_maximum(self):
        self.rollMoveData = 1.0
        self.heightMoveData = 1.0
        self.pitchMoveData = 1.0
        self.yawMoveData = 1.0

        self.pubRollData.publish(self.rollMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer X input")
        self.pubHeightData.publish(self.heightMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer Y input")
        self.pubPitchData.publish(self.pitchMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer z input")
        self.pubYawData.publish(self.yawMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer angular z input")

        # roll goes to linear.y
        # height goes to linear.z
        # pitch goes to linear.x
        # yaw goes to angular.z
        self.expectedCommand.linear.x = -1.0
        self.expectedCommand.linear.y = 1.0
        self.expectedCommand.linear.z = 1.0
        self.expectedCommand.angular.z = 1.0

        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 2)
            self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 2)
            self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 2)
            self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 2)
            self.assertEqual(self.receivedCommand, self.expectedCommand)
        else:
            self.assertTrue(False, msg="Never received data from the PID Multiplexer, timed out.")

    def test_3_when_all_inputs_are_at_max_negative_then_command_directions_are_at_minimum(self):

        self.rollMoveData = -1.0
        self.heightMoveData = -1.0
        self.pitchMoveData = -1.0
        self.yawMoveData = -1.0

        self.pubRollData.publish(self.rollMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer X input")
        self.pubHeightData.publish(self.heightMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer Y input")
        self.pubPitchData.publish(self.pitchMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer z input")
        self.pubYawData.publish(self.yawMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer angular z input")

        # roll goes to linear.y
        # height goes to linear.z
        # pitch goes to linear.x
        # yaw goes to angular.z
        self.expectedCommand.linear.x = 1.0
        self.expectedCommand.linear.y = -1.0
        self.expectedCommand.linear.z = -1.0
        self.expectedCommand.angular.z = -1.0

        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 2)
            self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 2)
            self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 2)
            self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 2)
            self.assertEqual(self.receivedCommand, self.expectedCommand)
        else:
            self.assertTrue(False, msg="Never received data from the PID Multiplexer, timed out.")

    def test_4_when_all_inputs_are_different(self):

        self.rollMoveData = 0.5
        self.heightMoveData = -0.2
        self.pitchMoveData = 0.85
        self.yawMoveData = 0.5

        self.pubRollData.publish(self.rollMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer X input")
        self.pubHeightData.publish(self.heightMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer Y input")
        self.pubPitchData.publish(self.pitchMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer z input")
        self.pubYawData.publish(self.yawMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer angular z input")

        # roll goes to linear.y
        # height goes to linear.z
        # pitch goes to linear.x
        # yaw goes to angular.z
        self.expectedCommand.linear.x = -0.85
        self.expectedCommand.linear.y = 0.5
        self.expectedCommand.linear.z = -0.2
        self.expectedCommand.angular.z = 0.5

        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 2)
            self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 2)
            self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 2)
            self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 2)
            self.assertEqual(self.receivedCommand, self.expectedCommand)
        else:
            self.assertTrue(False, msg="Never received data from the PID Multiplexer, timed out.")

    def test_5_when_pitch_is_in_deadband_and_other_values_normal(self):
        self.rollMoveData = 0.5
        self.heightMoveData = -0.2
        self.pitchMoveData = 0.01
        self.yawMoveData = 0.8

        self.pubRollData.publish(self.rollMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer X input")
        self.pubHeightData.publish(self.heightMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer Y input")
        self.pubPitchData.publish(self.pitchMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer z input")
        self.pubYawData.publish(self.yawMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer angular z input")

        # roll goes to linear.y
        # height goes to linear.z
        # pitch goes to linear.x
        # yaw goes to angular.z
        self.expectedCommand.linear.x = -0.01
        self.expectedCommand.linear.y = 0.5
        self.expectedCommand.linear.z = -0.2
        self.expectedCommand.angular.z = 0.8

        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 2)
            self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 2)
            self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 2)
            self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 2)
            self.assertEqual(self.receivedCommand, self.expectedCommand)
        else:
            self.assertTrue(False, msg="Never received data from the PID Multiplexer, timed out.")

    def test_6_when_all_directions_in_deadband_with_positive_values_first_time(self):
        self.rollMoveData = 0.03
        self.heightMoveData = 0.02
        self.pitchMoveData = 0.01
        self.yawMoveData = 0.02

        self.pubRollData.publish(self.rollMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer X input")
        self.pubHeightData.publish(self.heightMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer Y input")
        self.pubPitchData.publish(self.pitchMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer z input")
        self.pubYawData.publish(self.yawMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer angular z input")

        # roll goes to linear.y
        # height goes to linear.z
        # pitch goes to linear.x
        # yaw goes to angular.z
        self.expectedCommand.linear.x = 0.0
        self.expectedCommand.linear.y = 0.0
        self.expectedCommand.linear.z = 0.0
        self.expectedCommand.angular.z = 0.0

        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 2)
            self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 2)
            self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 2)
            self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 2)
            self.assertEqual(self.receivedCommand, self.expectedCommand)
        else:
            self.assertTrue(False, msg="Never received data from the PID Multiplexer, timed out.")

    def test_7_when_all_directions_in_deadband_with_positive_values_second_time(self):
        self.rollMoveData = 0.03
        self.heightMoveData = 0.02
        self.pitchMoveData = 0.01
        self.yawMoveData = 0.04

        self.pubRollData.publish(self.rollMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer X input")
        self.pubHeightData.publish(self.heightMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer Y input")
        self.pubPitchData.publish(self.pitchMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer z input")
        self.pubYawData.publish(self.yawMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer angular z input")

        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.assertTrue(False, msg="Received values from PID controllers, did not time out.")
        else:
            self.assertTrue(True, msg="Values never received, as expected.")

    def test_8_when_all_inputs_are_different_after_two_empty_commands(self):

        self.rollMoveData = 0.5
        self.heightMoveData = -0.2
        self.pitchMoveData = 0.85
        self.yawMoveData = 0.5

        self.pubRollData.publish(self.rollMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer X input")
        self.pubHeightData.publish(self.heightMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer Y input")
        self.pubPitchData.publish(self.pitchMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer z input")
        self.pubYawData.publish(self.yawMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer angular z input")

        # roll goes to linear.y
        # height goes to linear.z
        # pitch goes to linear.x
        # yaw goes to angular.z
        self.expectedCommand.linear.x = -0.85
        self.expectedCommand.linear.y = 0.5
        self.expectedCommand.linear.z = -0.2
        self.expectedCommand.angular.z = 0.5

        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 2)
            self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 2)
            self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 2)
            self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 2)
            self.assertEqual(self.receivedCommand, self.expectedCommand)
        else:
            self.assertTrue(False, msg="Never received data from the PID Multiplexer, timed out.")

    def test_9_when_all_directions_in_deadband_with_negative_values(self):
        self.rollMoveData = -0.03
        self.heightMoveData = -0.02
        self.pitchMoveData = -0.01
        self.yawMoveData = -0.01

        self.pubRollData.publish(self.rollMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer X input")
        self.pubHeightData.publish(self.heightMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer Y input")
        self.pubPitchData.publish(self.pitchMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer z input")
        self.pubYawData.publish(self.yawMoveData)
        rospy.loginfo(rospy.get_caller_id() + " Published to multiplexer angular z input")

        # roll goes to linear.y
        # height goes to linear.z
        # pitch goes to linear.x
        # yaw goes to angular.z
        self.expectedCommand.linear.x = 0.0
        self.expectedCommand.linear.y = 0.0
        self.expectedCommand.linear.z = 0.0
        self.expectedCommand.angular.z = 0.0

        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 2)
            self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 2)
            self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 2)
            self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 2)
            self.assertEqual(self.receivedCommand, self.expectedCommand)
        else:
            self.assertTrue(False, msg="Never received data from the PID Multiplexer, timed out.")


if __name__ == '__main__':
    rostest.rosrun("src", "test_pid_multiplexer", TestPIDMultiplexer)