#!/usr/bin/env python
import unittest
import time

from ardrone_autonomy.msg import Navdata
import rospy
import rostest
from std_msgs.msg import Float32
from std_srvs.srv import Empty as srv_Empty

from quadcopter.drone_states import DroneStates


class TestPIDControllerYaw(unittest.TestCase):
    def setUp(self):
        rospy.init_node('TestPIDControllerYaw')

        self.ns = rospy.get_namespace()
        self.pubData = rospy.Publisher(self.ns + "ardrone/navdata", Navdata, queue_size=1)
        self.subPIDData = rospy.Subscriber(self.ns + "quadcopter/pid/yaw", Float32, self.receive_data)

        rospy.wait_for_service(self.ns + 'quadcopter/pid/reset_yaw')
        rospy.wait_for_service(self.ns + 'quadcopter/pid/reset_set_point')
        self.reset = rospy.ServiceProxy(self.ns + 'quadcopter/pid/reset_yaw', srv_Empty)

        self.received_data = 0.0
        self.navdata = Navdata()
        self.navdata.state = DroneStates.Landed

        self.received = False

        # Allowing set up time for the PID controller
        rospy.sleep(0.5)

    def receive_data(self, sub_data):
        self.received_data = sub_data.data
        self.received = True

    def test_1_when_position_is_equal_to_set_point_then_output_is_zero(self):
        self.navdata.rotZ = 0.0  # yaw
        self.pubData.publish(self.navdata)
        rospy.sleep(0.04)
        self.navdata.state = DroneStates.Hovering
        self.pubData.publish(self.navdata)
        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.01)
        if self.received:
            self.received = False
            self.assertAlmostEqual(round(self.received_data, 2), 0.0, 2)
        else:
            self.assertTrue(False, msg="Never received data from the yaw PID controller, timed out.")

    def test_2_when_position_is_45_degrees_and_set_point_is_zero_then_output_is_negative(self):
        self.navdata.rotZ = 45.0  # yaw
        self.pubData.publish(self.navdata)
        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.01)
        if self.received:
            self.received = False
            self.assertAlmostEqual(round(self.received_data, 2), -0.2, 2)
        else:
            self.assertTrue(False, msg="Never received data from the yaw PID controller, timed out.")

    def test_3_when_position_is_minus_45_degrees_and_set_point_is_zero_then_output_is_positive(self):
        self.navdata.rotZ = -45.0  # yaw
        self.pubData.publish(self.navdata)
        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.01)
        if self.received:
            self.received = False
            self.assertAlmostEqual(round(self.received_data, 2), 0.22, 2)
        else:
            self.assertTrue(False, msg="Never received data from the yaw PID controller, timed out.")

    def test_4_when_position_is_minus_135_degrees_and_set_point_is_zero_then_output_is_positive(self):
        self.navdata.rotZ = -135.0  # yaw
        self.pubData.publish(self.navdata)
        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.01)
        if self.received:
            self.received = False
            self.assertAlmostEqual(round(self.received_data, 2), 0.55, 2)
        else:
            self.assertTrue(False, msg="Never received data from the yaw PID controller, timed out.")

    def test_5_when_position_is_minus_180_degrees_and_set_point_is_zero_then_output_is_positive(self):
        self.navdata.rotZ = -180.0  # yaw
        self.pubData.publish(self.navdata)
        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.01)
        if self.received:
            self.received = False
            self.assertAlmostEqual(round(self.received_data, 2), 0.73, 2)
        else:
            self.assertTrue(False, msg="Never received data from the yaw PID controller, timed out.")

    def test_6_when_position_is_minus_175_degrees_and_set_point_is_135_degrees_then_output_is_negative(self):
        self.navdata.rotZ = -175.0  # yaw
        rospy.set_param("/pid_yaw/set_point", 0.75)
        self.pubData.publish(self.navdata)
        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.01)
        if self.received:
            self.received = False
            self.assertAlmostEqual(round(self.received_data, 2), -0.60, 2)
        else:
            self.assertTrue(False, msg="Never received data from the yaw PID controller, timed out.")

    def test_7_when_position_is_135_degrees_and_set_point_is_minus_175_degrees_then_output_is_positive(self):
        self.navdata.rotZ = 135.0  # yaw
        rospy.set_param("/pid_yaw/set_point", -0.97222)
        self.pubData.publish(self.navdata)
        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.01)
        if self.received:
            self.received = False
            self.assertAlmostEqual(round(self.received_data, 2), 0.82, 2)
        else:
            self.assertTrue(False, msg="Never received data from the yaw PID controller, timed out.")

    def test_8_when_reset_everything_goes_back_to_zero(self):
        self.reset()
        self.navdata.rotZ = 0.0  # yaw
        self.pubData.publish(self.navdata)
        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.01)
        if self.received:
            self.received = False
            self.assertAlmostEqual(round(self.received_data, 2), 0.0, 2)
        else:
            self.assertTrue(False, msg="Never received data from the yaw PID controller, timed out.")


if __name__ == '__main__':
    rostest.rosrun("src", "test_pid_controller_yaw", TestPIDControllerYaw)