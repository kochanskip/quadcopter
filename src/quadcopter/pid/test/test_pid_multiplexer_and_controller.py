#!/usr/bin/env python
import unittest
import time

from ardrone_autonomy.msg import Navdata
from geometry_msgs.msg import Twist
import rospy
import rostest
from std_msgs.msg import Float32


class TestPIDMultiplexerAndController(unittest.TestCase):
    def setUp(self):

        rospy.init_node('TestPIDMultiplexerAndController')
        self.ns = rospy.get_namespace()
        self.subMoveCom = rospy.Subscriber(self.ns + "quadcopter/commands/move", Twist, self.receive_data)

        self.pubRollData = rospy.Publisher(self.ns + "quadcopter/tag_data/roll", Float32, queue_size=1)
        self.pubHeightData = rospy.Publisher(self.ns + "quadcopter/tag_data/height", Float32, queue_size=1)
        self.pubPitchData = rospy.Publisher(self.ns + "quadcopter/tag_data/pitch", Float32, queue_size=1)
        self.pubYawData = rospy.Publisher(self.ns + "ardrone/navdata", Navdata, queue_size=1)

        self.rollTagData = 0.0
        self.heightTagData = 0.0
        self.pitchTagData = 0.0
        self.navdata = Navdata()

        self.receivedCommand = Twist()
        self.expectedCommand = Twist()

        self.received = False

        # Allowing set up time for the PID controllers and the multiplexer
        rospy.sleep(0.5)

    def receive_data(self, sub_command):
        self.receivedCommand = sub_command
        self.received = True

    def test_1_when_all_pid_directions_have_neutral_set_points_then_no_directions_given_in_command(self):

        self.rollTagData = 500.0
        self.heightTagData = 500.0
        self.pitchTagData = 100.0
        self.navdata.rotZ = 0.0

        self.pubRollData.publish(self.rollTagData)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid X")
        self.pubHeightData.publish(self.heightTagData)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid Y")
        self.pubPitchData.publish(self.pitchTagData)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid z")
        self.pubYawData.publish(self.navdata)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid yaw")

        # roll goes to linear.y
        # height goes to linear.z
        # pitch goes to linear.x
        self.expectedCommand.linear.x = 0.0
        self.expectedCommand.linear.y = 0.0
        self.expectedCommand.linear.z = 0.0
        self.expectedCommand.angular.z = 0.0

        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 2)
            self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 2)
            self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 2)
            self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 2)
            self.assertEqual(self.receivedCommand, self.expectedCommand)
        else:
            self.assertTrue(False, msg="Never received data from the PID Multiplexer, timed out.")

    def test_2_when_all_pid_directions_are_less_than_set_points_then_positive_command_directions_given(self):

        self.rollTagData = 300.0
        self.heightTagData = 300.0
        self.pitchTagData = 50.0
        self.navdata.rotZ = 45.0

        self.pubRollData.publish(self.rollTagData)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid X")
        self.pubHeightData.publish(self.heightTagData)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid Y")
        self.pubPitchData.publish(self.pitchTagData)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid z")
        self.pubYawData.publish(self.navdata)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid yaw")

        # roll goes to linear.x
        # pitch goes to linear.z
        # height goes to linear.y
        self.expectedCommand.linear.x = -0.2
        self.expectedCommand.linear.y = 0.16
        self.expectedCommand.linear.z = 0.16
        self.expectedCommand.angular.z = 0.0

        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 2)
            self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 2)
            self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 2)
            self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 2)
            self.assertEqual(self.receivedCommand, self.expectedCommand)
        else:
            self.assertTrue(False, msg="Never received data from the PID Multiplexer, timed out.")

    def test_3_when_all_pid_directions_are_greater_than_set_points_then_negative_command_directions_given(self):

        self.rollTagData = 700.0
        self.heightTagData = 700.0
        self.pitchTagData = 150.0
        self.navdata.rotZ = -45.0

        self.pubRollData.publish(self.rollTagData)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid X")
        self.pubHeightData.publish(self.heightTagData)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid Y")
        self.pubPitchData.publish(self.pitchTagData)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid z")
        self.pubYawData.publish(self.navdata)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid yaw")

        # roll goes to linear.y
        # height goes to linear.z
        # pitch goes to linear.x
        self.expectedCommand.linear.x = 0.22
        self.expectedCommand.linear.y = -0.18
        self.expectedCommand.linear.z = -0.18
        self.expectedCommand.angular.z = 0.0

        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 2)
            self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 2)
            self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 2)
            self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 2)
            self.assertEqual(self.receivedCommand, self.expectedCommand)
        else:
            self.assertTrue(False, msg="Never received data from the PID Multiplexer, timed out.")

    def test_4_when_all_pid_directions_are_set_back_to_set_points_then_near_neutral_command_directions_given(self):
        # THE MEANING OF THIS TEST MAY NEED REVISING

        self.rollTagData = 500.0
        self.heightTagData = 500.0
        self.pitchTagData = 100.0
        self.navdata.rotZ = -135.0

        self.pubRollData.publish(self.rollTagData)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid X")
        self.pubHeightData.publish(self.heightTagData)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid Y")
        self.pubPitchData.publish(self.pitchTagData)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid z")
        self.pubYawData.publish(self.navdata)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid yaw")

        # roll goes to linear.y
        # height goes to linear.z
        # pitch goes to linear.x
        self.expectedCommand.linear.x = -0.05
        self.expectedCommand.linear.y = 0.04
        self.expectedCommand.linear.z = 0.04
        self.expectedCommand.angular.z = 0.0

        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 2)
            self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 2)
            self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 2)
            self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 2)
            self.assertEqual(self.receivedCommand, self.expectedCommand)
        else:
            self.assertTrue(False, msg="Never received data from the PID Multiplexer, timed out.")

    def test_5_when_all_pid_directions_are_in_max_positive_position_then_negative_commands_given(self):

        self.rollTagData = 1000.0
        self.heightTagData = 1000.0
        self.pitchTagData = 200.0
        self.navdata.rotZ = -180.0

        self.pubRollData.publish(self.rollTagData)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid X")
        self.pubHeightData.publish(self.heightTagData)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid Y")
        self.pubPitchData.publish(self.pitchTagData)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid z")
        self.pubYawData.publish(self.navdata)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid yaw")

        # roll goes to linear.y
        # height goes to linear.z
        # pitch goes to linear.x
        self.expectedCommand.linear.x = 0.4
        self.expectedCommand.linear.y = -0.4
        self.expectedCommand.linear.z = -0.4
        self.expectedCommand.angular.z = 0.0

        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 2)
            self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 2)
            self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 2)
            self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 2)
            self.assertEqual(self.receivedCommand, self.expectedCommand)
        else:
            self.assertTrue(False, msg="Never received data from the PID Multiplexer, timed out.")

    def test_6_when_all_pid_directions_are_in_max_negative_position(self):

        self.rollTagData = 0.0
        self.heightTagData = 0.0
        self.pitchTagData = 0.0
        self.navdata.rotZ = -175.0
        rospy.set_param("/pid_yaw/set_point", 0.75)

        self.pubRollData.publish(self.rollTagData)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid X")
        self.pubHeightData.publish(self.heightTagData)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid Y")
        self.pubPitchData.publish(self.pitchTagData)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid z")
        self.pubYawData.publish(self.navdata)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid yaw")

        # roll goes to linear.y
        # height goes to linear.z
        # pitch goes to linear.x
        self.expectedCommand.linear.x = -0.45
        self.expectedCommand.linear.y = 0.45
        self.expectedCommand.linear.z = 0.45
        self.expectedCommand.angular.z = -0.0

        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 2)
            self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 2)
            self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 2)
            self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 2)
            self.assertEqual(self.receivedCommand, self.expectedCommand)
        else:
            self.assertTrue(False, msg="Never received data from the PID Multiplexer, timed out.")

    def test_7_when_tag_is_not_present_taking_into_account_previous_test(self):

        self.rollTagData = -1.0
        self.heightTagData = -1.0
        self.pitchTagData = -1.0
        self.navdata.rotZ = 135.0
        rospy.set_param("/pid_yaw/set_point", -0.97222)

        self.pubRollData.publish(self.rollTagData)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid X")
        self.pubHeightData.publish(self.heightTagData)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid Y")
        self.pubPitchData.publish(self.pitchTagData)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid z")
        self.pubYawData.publish(self.navdata)
        rospy.loginfo(rospy.get_caller_id() + " Published to pid yaw")

        # roll goes to linear.y
        # height goes to linear.z
        # pitch goes to linear.x
        self.expectedCommand.linear.x = -0.3
        self.expectedCommand.linear.y = 0.3
        self.expectedCommand.linear.z = 0.3
        self.expectedCommand.angular.z = 0.0

        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.receivedCommand.linear.x = round(self.receivedCommand.linear.x, 2)
            self.receivedCommand.linear.y = round(self.receivedCommand.linear.y, 2)
            self.receivedCommand.linear.z = round(self.receivedCommand.linear.z, 2)
            self.receivedCommand.angular.z = round(self.receivedCommand.angular.z, 2)
            self.assertEqual(self.receivedCommand, self.expectedCommand)
        else:
            self.assertTrue(False, msg="Never received data from the PID Multiplexer, timed out.")


if __name__ == '__main__':
    rostest.rosrun("src", "test_pid_multiplexer_and_controller", TestPIDMultiplexerAndController)