#!/usr/bin/env python
import unittest
import time

import rospy
import rostest
from std_msgs.msg import Float32
from std_srvs.srv import Empty as srv_Empty


class TestPIDController(unittest.TestCase):
    def setUp(self):
        rospy.init_node('TestPIDController')

        self.ns = rospy.get_namespace()
        self.pubTagData = rospy.Publisher(self.ns + "quadcopter/tag_data/roll", Float32, queue_size=1)
        self.subPIDData = rospy.Subscriber(self.ns + "quadcopter/pid/roll", Float32, self.receive_data)

        rospy.wait_for_service(self.ns + 'quadcopter/pid/reset_roll')
        self.reset = rospy.ServiceProxy(self.ns + 'quadcopter/pid/reset_roll', srv_Empty)

        self.rollTagData = 0.0
        self.received_data = 0.0

        self.received = False

        # Allowing set up time for the PID controller
        rospy.sleep(0.5)

    def receive_data(self, sub_data):
        self.received_data = sub_data.data
        self.received = True

    def test_1_when_tag_data_is_lower_than_set_point_then_output_is_positive(self):
        self.rollTagData = 300.0
        self.pubTagData.publish(self.rollTagData)
        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.assertAlmostEqual(round(self.received_data, 2), 0.16, 2)
        else:
            self.assertTrue(False, msg="Never received data from the PID controller, timed out.")

    def test_2_when_tag_data_is_higher_than_set_point_then_output_is_negative(self):
        self.rollTagData = 700.0
        self.pubTagData.publish(self.rollTagData)
        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.assertAlmostEqual(round(self.received_data, 2), -0.18, 2)
        else:
            self.assertTrue(False, msg="Never received data from the PID controller, timed out.")

    def test_3_when_tag_data_is_equal_to_set_point_after_receiving_pos_and_neg_results_then_output_is_positive(self):
        self.rollTagData = 500.0
        self.pubTagData.publish(self.rollTagData)
        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.assertAlmostEqual(round(self.received_data, 2), 0.04, 2)
        else:
            self.assertTrue(False, msg="Never received data from the PID controller, timed out.")

    def test_4_when_tag_data_is_set_to_max_then_output_is_negative(self):
        self.rollTagData = 1000.0
        self.pubTagData.publish(self.rollTagData)
        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.assertAlmostEqual(round(self.received_data, 2), -0.4, 2)
        else:
            self.assertTrue(False, msg="Never received data from the PID controller, timed out.")

    def test_5_when_tag_data_is_set_to_min_then_output_is_positive(self):
        self.rollTagData = 0.0
        self.pubTagData.publish(self.rollTagData)
        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.assertAlmostEqual(round(self.received_data, 2), 0.45, 2)
        else:
            self.assertTrue(False, msg="Never received data from the PID controller, timed out.")

    def test_6_when_tag_data_is_a_negative_value_then_same_positive_direction_is_retained(self):
        self.rollTagData = -1.0
        self.pubTagData.publish(self.rollTagData)
        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.assertAlmostEqual(round(self.received_data, 2), 0.3, 2)
        else:
            self.assertTrue(False, msg="Never received data from the PID controller, timed out.")

    def test_7_when_reset_sent(self):
        self.reset()
        self.rollTagData = 500.0
        self.pubTagData.publish(self.rollTagData)
        start_time = time.time()
        while not self.received and not time.time() > start_time + 3:
            rospy.sleep(0.5)
        if self.received:
            self.received = False
            self.assertAlmostEqual(round(self.received_data, 2), 0.0, 2)
        else:
            self.assertTrue(False, msg="Never received data from the PID controller, timed out.")


if __name__ == '__main__':
    rostest.rosrun("src", "test_pid_controller", TestPIDController)