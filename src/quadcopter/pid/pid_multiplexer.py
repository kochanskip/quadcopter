#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist
from std_msgs.msg import Float32

COMMAND_PERIOD = 10  # 10ms or 100 Hz

# Check if input is larger than a minimum accepted size else it is ignored and set to 0
def deadband(received_speed, accepted_speed):
    if (received_speed < -accepted_speed) or (received_speed > accepted_speed):
        return received_speed
    else:
        return 0.0


# Combines PID outputs into a single command
class PidMultiplexer(object):
    def __init__(self):
        rospy.init_node('PidMultiplexer')
        self.ns = rospy.get_namespace()
        self.subPIDRoll = rospy.Subscriber(self.ns + 'quadcopter/pid/roll', Float32, self.receive_pid_roll)
        self.subPIDHeight = rospy.Subscriber(self.ns + 'quadcopter/pid/height', Float32, self.receive_pid_height)
        self.subPIDPitch = rospy.Subscriber(self.ns + 'quadcopter/pid/pitch', Float32, self.receive_pid_pitch)
        self.subPIDYaw = rospy.Subscriber(self.ns + 'quadcopter/pid/yaw', Float32, self.receive_pid_yaw)
        self.pubMoveCommand = rospy.Publisher(self.ns + 'quadcopter/commands/move', Twist, queue_size=1)

        self.pid_output_horizontal = 0.0
        self.pid_output_vertical = 0.0
        self.pid_output_distance = 0.0
        self.pid_output_yaw = 0.0

        self.roll_flag = False
        self.height_flag = False
        self.pitch_flag = False

        self.roll_speed = rospy.get_param("~roll_speed")
        self.height_speed = rospy.get_param("~height_speed")
        self.pitch_speed = rospy.get_param("~pitch_speed")
        self.yaw_speed = rospy.get_param("~yaw_speed")
        self.sent_zero = False

        self.commandTimer = rospy.Timer(rospy.Duration(COMMAND_PERIOD / 1000.0), self.send_move_command)

        rospy.spin()

    def receive_pid_roll(self, pid_horizontal):
        self.pid_output_horizontal = pid_horizontal.data
        self.roll_flag = True

    def receive_pid_height(self, pid_vertical):
        self.pid_output_vertical = pid_vertical.data
        self.height_flag = True

    def receive_pid_pitch(self, pid_distance):
        self.pid_output_distance = pid_distance.data * -1.0
        self.pitch_flag = True

    def receive_pid_yaw(self, pid_yaw):
        self.pid_output_yaw = pid_yaw.data

    def send_move_command(self, _):
        # Check to make sure we have received new values
        if self.roll_flag and self.height_flag and self.pitch_flag:
            command = Twist()
            command.linear.x = deadband(self.pid_output_distance, self.pitch_speed)
            command.linear.y = deadband(self.pid_output_horizontal, self.roll_speed)
            command.linear.z = deadband(self.pid_output_vertical, self.height_speed)
            command.angular.z = deadband(self.pid_output_yaw, self.yaw_speed)

            # If the last command was 0, don't resend it to allow the drone to hover correctly
            if command.linear.x == 0.0 and command.linear.y == 0.0 and \
                            command.linear.z == 0.0 and command.angular.z == 0.0:
                if not self.sent_zero:
                    self.pubMoveCommand.publish(command)
                    self.sent_zero = True
            else:
                self.sent_zero = False
                command.linear.x = self.pid_output_distance
                command.linear.y = self.pid_output_horizontal
                command.linear.z = self.pid_output_vertical
                command.angular.z = self.pid_output_yaw
                self.pubMoveCommand.publish(command)

            self.roll_flag = False
            self.height_flag = False
            self.pitch_flag = False


if __name__ == '__main__':
    try:
        PidMultiplexer()
    except rospy.ROSInterruptException, e:
        rospy.logfatal(rospy.get_caller_id() + ' ' + e.message)

