#!/usr/bin/env python
import math

from ardrone_autonomy.msg import Navdata
import rospy
from std_msgs.msg import Float32
from std_srvs.srv import Empty, EmptyResponse

from quadcopter.drone_states import DroneStates

# Maximum number of cycles to wait to allow for magnetometer settling
YAW_TIMEOUT = 200
# Minimum number of cycles to wait to allow for magnetometer settling
YAW_SETTLE_TIME = 50
# Permitted PID error to allow before accepting at startup
YAW_ALLOWED_ERROR = 0.01

# Normalises and corrects input to allow correct behaviour around 0-360
def determine_error(set_point, position):
    error = set_point - position
    if error < -0.5:
        error += 1.0
    elif error > 0.5:
        error -= 1.0
    return error


def update_set_point(new_point):
    rospy.set_param("~set_point", new_point)


# Specialised PID controller for handling control of drone yaw
class PIDControllerYaw(object):
    def __init__(self):
        rospy.init_node('PIDControllerYaw')

        self.ns = rospy.get_namespace()

        self.pubPIDData = rospy.Publisher(self.ns + rospy.get_param("~pub_name"), Float32, queue_size=1)
        self.subNavData = rospy.Subscriber(self.ns + rospy.get_param("~sub_name"), Navdata,
                                           self.update_rotation)
        rospy.Service(self.ns + rospy.get_param("~reset_srv"), Empty, self.reset)
        rospy.Service(self.ns + rospy.get_param("~reset_set_point"), Empty, self.reset_set_point)

        self.rot_z = 0.0
        self.received_rot_z = False
        self.range = rospy.get_param("~range")

        self.error = 0.0
        self.integral = 0.0
        self.previous_error = 0.0
        self.initialised = False

        self.can_publish = False
        self.counter = 0

        # Tuning parameters
        self.kp = rospy.get_param("~kp")  # proportional gain
        self.ki = rospy.get_param("~ki")  # integral gain
        self.kd = rospy.get_param("~kd")  # derivative gain

        # Publishes on a timer to reduce frequency of output
        self.timer = rospy.Timer(rospy.Duration(0.04), self.publish_value)

        rospy.spin()

    def update_rotation(self, data):
        # Consider navdata to be setup once we have received this state
        if data.state == DroneStates.Landed:
            self.initialised = True

        if self.initialised:
            self.rot_z = float(data.rotZ)

            # Once we are airborne, but not yet setup
            if (data.state == DroneStates.Hovering or data.state == DroneStates.Flying) and not self.can_publish:
                position = (self.rot_z + 180.0) / self.range
                self.error = determine_error(rospy.get_param("~set_point"), position)

                # Check if if the current sensor value is within the drift allowance of the set point,
                # or if we have timed out we reset the set point.
                if (math.fabs(self.error) < YAW_ALLOWED_ERROR and self.counter > YAW_SETTLE_TIME) \
                        or self.counter > YAW_TIMEOUT:
                    if self.counter > YAW_TIMEOUT:
                        self.reset_set_point(None)
                    self.can_publish = True
                    rospy.loginfo(rospy.get_caller_id() + " Counter Value: %s" % self.counter)
                else:
                    self.counter += 1

            # Set the set point at startup or if its being reset
            if not self.received_rot_z:
                self.received_rot_z = True
                rospy.set_param("~set_point", ((self.rot_z + 180.0) / self.range))

    def publish_value(self, _):
        if self.can_publish:
            # Normalise based on the configured range of inputs.
            position = (self.rot_z + 180.0) / self.range
            self.error = determine_error(rospy.get_param("~set_point"), position)

            p_value = self.kp * self.error
            self.integral += self.error
            i_value = self.integral * self.ki
            d_value = self.kd * (self.error - self.previous_error)
            self.previous_error = self.error

            if (p_value + i_value + d_value) == 0.0:
                pid = 0.0
            else:
                pid = (p_value + i_value + d_value)
            if pid > 1:
                pid = 1
            elif pid < -1:
                pid = -1

            self.pubPIDData.publish(pid)

    def reset(self, data):
        rospy.loginfo(rospy.get_caller_id() + " Resetting PID Controller")
        self.error = 0.0
        self.integral = 0.0
        self.previous_error = 0.0
        self.can_publish = False
        self.counter = 0

        return EmptyResponse()

    def reset_set_point(self, _):
        rospy.loginfo(rospy.get_caller_id() + " YAW set point reset")
        self.received_rot_z = False
        return EmptyResponse()


if __name__ == '__main__':
    try:
        PIDControllerYaw()
    except rospy.ROSInterruptException, e:
        rospy.logfatal(rospy.get_caller_id() + ' ' + e.message)