from distutils.core import setup

setup(
    name='quadcopter',
    version='0.1.0',
    packages=['quadcopter',
              'quadcopter.pid',
              'quadcopter.pid.test',
              'quadcopter.data',
              'quadcopter.command',
              'quadcopter.command.test',
              'quadcopter.filter'],
    package_dir={'': 'src'},
    url='',
    license='',
    author='',
    author_email='',
    description='', requires=['numpy', 'PySide']
)
