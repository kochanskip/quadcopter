import numpy

import cv2
# Run with python to convert values ins "colour" from RGB to HSV values.

if __name__ == '__main__':
    colour = numpy.uint8([[[41, 255, 0]]])
    hsv_colour = cv2.cvtColor(colour, cv2.COLOR_RGB2HSV)
    print hsv_colour