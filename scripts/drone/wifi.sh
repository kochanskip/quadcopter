#!/bin/sh
# Place this script on a drone with ip address set to the desired IP and with the correct ESSID.
# Run it via telnet to connect the drone.
killall udhcpd
ifconfig ath0 down
iwconfig ath0 mode managed essid DroneNet
ifconfig ath0 192.168.10.10 netmask 255.255.255.0 up
